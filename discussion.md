Ongelmat:

- Optimaalisin reitti
    - Onko aina lyhyin?
    - Voiko ulkokaarteessa ajaa lujempaa kuin sisäkaarteessa joissakin tilanteissa?
    - Ei saa olettaa jotain tiettyä lähtöpaikka

- Oikean "cornering speedin" löytäminen
    - carPosition.angle

- Kisastrategia
    - Ohitukset?
      - Optimaaliselta reitiltä kannattaa poiketa vain, jos ohitus on varma
    - Törmäämiset?
      - Mutkassa
      - Kaistanvaihdossa
    - Työntäminen / peesaaminen?

- Suorituskyky
    - Suorituskyvyn valvonta (testikierroksille), jotta voidaan varmistaa, että pystytään 60hz
    - Mikä on nopeuden ja throtlen suhde
    - Vaikuttaako kitka

- Maksimivauhti

- Jarruttaminen?

- Voiko kaistanvaihdon perua vaihtamalla väärään suuntaan

Rakenne:
    - Radan parametrien hakeminen
        - Maksimikulmanopeus
        - Maksimikiihtyvyys
        - Maksimihidastuvuus

    - Strategian määrittäminen

    - Strategian toteuttaminen

Tavoitteenasetanta:

-

Työnjako:

Yksiköt:
points / ticks
