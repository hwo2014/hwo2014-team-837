package fi.poo.hwo.protocol;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;

public class TurboAvailableTest {
	private String msg = "{\"msgType\":\"turboAvailable\",\"data\":{\"turboDurationMilliseconds\":500.0,\"turboDurationTicks\":30,\"turboFactor\":3.0}}";

	@Test
	public void getTurboAvailable() {
		TurboAvailable turboMsg = new Gson().fromJson(msg, TurboAvailable.class);
		Assert.assertEquals(500.0,turboMsg.getDurationMillis(),0.0);
		Assert.assertEquals(30, turboMsg.getDurationTicks());
		Assert.assertEquals(3.0,turboMsg.getFactor(),0.0);
	}
}
