package fi.poo.hwo.protocol;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.common.base.Optional;
import com.google.gson.Gson;

import fi.poo.hwo.protocol.CarPositions.CarStateJson;
import fi.poo.hwo.protocol.CarPositions.PiecePosition;

public class CarPositionsTest {
	private String msg = "{\"msgType\":\"carPositions\",\"data\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"angle\":0.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":0.0,\"lane\":{\"startLaneIndex\":0,\"endLaneIndex\":0},\"lap\":0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"angle\":45.0,\"piecePosition\":{\"pieceIndex\":0,\"inPieceDistance\":20.0,\"lane\":{\"startLaneIndex\":1,\"endLaneIndex\":1},\"lap\":0}}],\"gameId\":\"OIUHGERJWEOI\",\"gameTick\":0}";

	@Test
	public void getGameId() {
		CarPositions pos = initCarPos();
		Assert.assertEquals("OIUHGERJWEOI",pos.getGameId());
	}
	
	@Test
	public void getGameTick() {
		CarPositions pos = initCarPos();
		Assert.assertEquals(Optional.of(0),pos.getGameTick());
	}
	
	
	@Test
	public void getCarPositions() {
		CarPositions pos = initCarPos();
		List<CarStateJson> states = pos.getCarStates();
		
		Assert.assertEquals(2,states.size());
		
		CarStateJson firstCar = states.get(0);
		CarStateJson secondCar = states.get(1);
		
		assertCarId("Schumacher", "red", firstCar);
		assertCarId("Rosberg", "blue", secondCar);
		
		assertPiecePositions(0.0, 0, 0, firstCar);
		assertPiecePositions(20.0, 1, 1, secondCar);
		
		Assert.assertEquals(0.0, firstCar.getAngle(), 0.0);
		Assert.assertEquals(45.0, secondCar.getAngle(), 0.0);
	}

	private void assertCarId(String name, String color,
			CarStateJson car) {
		Assert.assertEquals(name, car.getId().getName());
		Assert.assertEquals(color, car.getId().getColor());
	}	

	private void assertPiecePositions(double inPieceDistance,int startLane, int endLane, CarStateJson car) {
		PiecePosition pos = car.getPiecePosition();
		Assert.assertEquals(0,pos.getIndex());
		Assert.assertEquals(inPieceDistance, pos.getInPieceDistance(),0.0);
		Assert.assertEquals(startLane, pos.getStartLane());
		Assert.assertEquals(endLane, pos.getEndLane());
		Assert.assertEquals(0, pos.getLap());
	}
	
	private CarPositions initCarPos() {
		return new Gson().fromJson(msg, CarPositions.class);
	}
}
