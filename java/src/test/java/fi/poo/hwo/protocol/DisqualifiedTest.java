package fi.poo.hwo.protocol;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;

import fi.poo.hwo.Car;

public class DisqualifiedTest {
	private String msg = "{\"msgType\":\"dnf\",\"data\":{\"car\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"reason\":\"disconnected\"},\"gameId\":\"OIUHGERJWEOI\",\"gameTick\":650}";
	
	@Test
	public void msg() {
		Disqualified dq = new Gson().fromJson(msg, Disqualified.class);
		
		Assert.assertEquals(new Car.Id("Rosberg", "blue"), dq.getCarId());
		Assert.assertEquals("disconnected", dq.getReason());
		Assert.assertEquals("OIUHGERJWEOI", dq.getGameId());
		Assert.assertEquals(new Integer(650), dq.getGameTick()); 
		
	}
}
