package fi.poo.hwo.protocol;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;

import fi.poo.hwo.Car;
import fi.poo.hwo.Lane;
import fi.poo.hwo.Piece;
import fi.poo.hwo.Track;
import fi.poo.hwo.protocol.GameInit.RaceSession;


public class GameInitTest {
	private String msg = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"indianapolis\",\"name\":\"Indianapolis\",\"pieces\":[{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":200,\"angle\":22.5}],\"lanes\":[{\"distanceFromCenter\":-20,\"index\":0},{\"distanceFromCenter\":0,\"index\":1},{\"distanceFromCenter\":20,\"index\":2}],\"startingPoint\":{\"position\":{\"x\":-340.0,\"y\":-96.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"Schumacher\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}},{\"id\":{\"name\":\"Rosberg\",\"color\":\"blue\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":30000,\"quickRace\":true}}}}";
	private Gson gson = new Gson();
	
	@Test
	public void getLanesFromGameInit() {
		Track track = getTrack();
		
		List<Lane> lanes = track.getLanes();
		Assert.assertEquals(3, lanes.size());
		
		int distance = -20;
		int index = 0;
		for(Lane lane : lanes) {
			Assert.assertEquals(distance + index * 20, lane.getDistanceFromCenter(),0.0);
			Assert.assertEquals(index++, lane.getIndex());
		}
		
	}
	
	@Test
	public void getPiecesFromGamesInit() {
		Track track = getTrack();
		
		List<Piece> pieces = track.getPieces();
		Assert.assertEquals(3, pieces.size());
	
		for(int i = 0; i < pieces.size(); i++) {
			Piece piece = pieces.get(i);
			Assert.assertEquals(i,piece.getIndex());
			if(!piece.isCorner()) {
				Assert.assertEquals(100, piece.getLength(0),0.0);
			} else {
				Assert.assertEquals(220,piece.getRadius(0),0.0);	
			}
		}
	}
	
	@Test
	public void getTrackName() {
		Track track = getTrack();
		
		Assert.assertEquals("indianapolis", track.getId());
		Assert.assertEquals("Indianapolis", track.getName());
	}
	
	@Test
	public void parseCars() {
		List<Car> cars = getCars();
		
	 	Assert.assertEquals(2,cars.size());
		
		Assert.assertEquals("red",cars.get(0).getColor());
		Assert.assertEquals("blue",cars.get(1).getColor());
		
		Assert.assertEquals("Schumacher",cars.get(0).getName());
		Assert.assertEquals("Rosberg",cars.get(1).getName());
		
		Assert.assertEquals(40.0,cars.get(0).getLength(),0.0);
		Assert.assertEquals(40.0,cars.get(1).getLength(),0.0);

		Assert.assertEquals(20.0,cars.get(0).getWidth(),0.0);
		Assert.assertEquals(20.0,cars.get(1).getWidth(),0.0);
	}
	
	@Test
	public void parseSession() {
		GameInit init = gson.fromJson(msg, GameInit.class);
		RaceSession session = init.getRaceSession();
		
		Assert.assertEquals(3, session.getLaps());
		Assert.assertEquals(30000, session.getMaxLapTimeMs());
		Assert.assertTrue(session.isQuickRace());
		
	}

	private Track getTrack() {
		GameInit init = gson.fromJson(msg, GameInit.class);
		return init.getTrack();
	}
	
	private List<Car> getCars() {
		GameInit init = gson.fromJson(msg, GameInit.class);
		return init.getCars();
	}
}
