package fi.poo.hwo.protocol;


import org.junit.Assert;
import org.junit.Test;

import com.google.gson.Gson;

public class YourCarTest {
	private String msg = "{\"msgType\":\"yourCar\",\"data\":{\"name\":\"Schumacher\",\"color\":\"red\"}}";
	
	@Test
	public void getYourCar() {
		YourCar myCar = new Gson().fromJson(msg, YourCar.class);
		
		Assert.assertEquals("Schumacher", myCar.getId().getName());
		Assert.assertEquals("red", myCar.getId().getColor());
	}
}
