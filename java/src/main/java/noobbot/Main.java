package noobbot;

import java.io.IOException;

import com.google.common.base.Strings;

import fi.poo.hwo.GameBot;

public class Main {
	public static void main(String... args) throws IOException,
			InterruptedException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		//String botName = args[2];
		//Testing CI
		String botName = "Poo";
		String botKey = args[3];
		
		System.out.println("Connecting to " + host + ":" + port + " as "
				+ botName + "/" + botKey);

		GameBot gameBot = new GameBot(botName, botKey, host, port);

		while (true) {
			System.out.println("Main loop running...");
			Thread.sleep(10000);
		}
	}
}