package fi.poo.hwo.tools;

import java.util.concurrent.Executor;

import com.google.common.base.Function;
import com.google.common.base.Optional;

public interface Property<T> {

	public interface PropertyListener<T> {
		/**
		 * @param oldValue Optional.absent() on start-notification. The previous value on actual change.
		 * @param newValue
		 */
		void changed(Optional<T> oldValue, T newValue);
	}
	
	/**
	 * The listener will be immediately given a start-notification with the current value. All notifications are run in the given executor.
	 * @param listener
	 * @param executor
	 */
	void addListener(PropertyListener<T> listener, Executor executor);
	
	void removeListener(PropertyListener<T> listener);
	
	T get();
	
	<S> Property<S> map(Function<T, S> mapper);
}
