package fi.poo.hwo.tools;

import java.util.ArrayList;
import java.util.List;

public final class Shutdown {
	
	private static final List<Runnable> TASKS = new ArrayList<>();


	static {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				doShutdown();
			}
		},"Shutdown"));
	}
	
	public static synchronized void addShutdownTask(Runnable task) {
		TASKS.add(task);
	}


	private static synchronized void doShutdown() {
		for(Runnable task : TASKS ) {
			try {
				task.run();
			}
			catch( Exception e) {
				e.printStackTrace();
			}
		}
	}
}
