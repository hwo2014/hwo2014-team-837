package fi.poo.hwo.tools;

import java.util.concurrent.Executor;

public final class Immediate implements Executor {

	@Override
	public void execute(Runnable command) {
		command.run();
	}

}
