package fi.poo.hwo.tools;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

public abstract class Actor {
	private final ListeningExecutorService executor;
	private final String name;

	protected Actor() {
		this(null);
	}

	protected Actor(String name) {
		if(name == null) {
			this.name = getClass().getSimpleName();
		}
		else {
			this.name = name;
		}
		this.executor = MoreExecutors.listeningDecorator(Executors.newSingleThreadExecutor(new ThreadFactory() {
			@Override
			public Thread newThread(Runnable runnable) {
				System.out.println("Creating new Executor Thread with name " + Actor.this.name);
				return new Thread(runnable, Actor.this.name);
			}
		}));
		Shutdown.addShutdownTask(new Runnable() {
			@Override
			public void run() {
				executor.shutdownNow();
				try {
					System.out.println("Shutting down " + Actor.this.name);
					executor.awaitTermination(500l, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					logAndRethrow(e);
				}
			}
		});
	}

	protected ListenableFuture<?> submit(final Runnable runnable) {
		final Exception justForKicks = new IllegalAccessException("where it all began...");
		try {
			return executor.submit(new Runnable() {
				@Override
				public void run() {
					try {
						runnable.run();
					}
					catch(Exception e) {
						justForKicks.printStackTrace();
						logAndRethrow(e);
					}
				}
			});
		}
		catch(Exception e) {
			e.printStackTrace();
			return Futures.immediateFailedFuture(e);
		}
	}

	protected <T> ListenableFuture<T> submit(final Callable<T> callable) {
		final Exception justForKicks = new IllegalAccessException("where it all began...");
		try {
			return executor.submit(new Callable<T>() {
				@Override
				public T call() throws Exception {
					try {
						return callable.call();
					}
					catch(Exception e) {
						justForKicks.printStackTrace();
						logAndRethrow(e);
						return null;
					}
				}
			});
		}
		catch(Exception e) {
			e.printStackTrace();
			return Futures.immediateFailedFuture(e);
		}
	}

	protected String getName() {
		return name;
	}

	protected ListeningExecutorService getExecutor() {
		return executor;
	}

	protected <ReturnType, ParamType> ListenableFuture<ReturnType> submitWithParameter(final Callable<ParamType> paramFetcher, final ParamCallable<ReturnType, ParamType> callBack) {
		return submit(new Callable<ReturnType>() {
			@Override
			public ReturnType call() throws Exception {
				try {
					return callBack.call(paramFetcher.call());
				}
				catch(Exception e) {
					logAndRethrow(e);
					return null;
				}
			}
		});
	}

	protected <ParamType> ListenableFuture<?> submitWithParameter(final Callable<ParamType> paramFetcher, final ParamRunnable<ParamType> callBack) {
		return submit(new Runnable() {
			@Override
			public void run() {
				try {
					callBack.run(paramFetcher.call());
				} catch (Exception e) {
					logAndRethrow(e);
				}
			}
		});
	}

	protected interface ParamCallable<ReturnType, ParamType> {
		ReturnType call(ParamType param);
	}

	protected interface ParamRunnable<ParamType> {
		void run(ParamType param);
	}

	private void logAndRethrow(Throwable problem) {
		log(problem);
		if(problem instanceof RuntimeException) {
			throw (RuntimeException)problem;
		}
		else {
			throw new RuntimeException(problem);
		}
	}

	private void log(Throwable problem) {
		Throwable toPrint = problem;
		while(toPrint != null) {
			toPrint.printStackTrace();
			toPrint = toPrint.getCause();
		}
	}
}
