package fi.poo.hwo.tools;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.WeakHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicReference;

import com.google.common.base.Function;
import com.google.common.base.Optional;

public class RWProperty<T> implements Property<T> {
	
	private final Map<PropertyListener<T>,Executor> listeners = new WeakHashMap<>();
	private final AtomicReference<T> value = new AtomicReference<>();
	
	public RWProperty(T startValue) {
		value.set(startValue);
	}
	
	@Override
	public void addListener(PropertyListener<T> listener, Executor executor) {
		synchronized (listeners) {
			listeners.put(listener, executor);
			notify(listener, executor, Optional.<T>absent(), get());
		}
	}

	@Override
	public void removeListener(PropertyListener<T> listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}
	}

	@Override
	public T get() {
		return value.get();
	}
	
	@Override
	public <S> Property<S> map(Function<T, S> mapper) {
		return MappedProperty.create(this, mapper);
	}

	public void set(T newValue) {
		synchronized (listeners) {
			T oldValue = value.getAndSet(newValue);
			if(!newValue.equals(oldValue)) {
				notify(Optional.of(oldValue), newValue);
			}
		}
	}
	
	private void notify(Optional<T> oldValue, T newValue) {
		Map<PropertyListener<T>,Executor> listenersTemp = new HashMap<>(listeners);
		for(Entry<PropertyListener<T>,Executor> listener : listenersTemp.entrySet()) {
			notify(listener.getKey(), listener.getValue(), oldValue, newValue);
		}
	}

	private void notify(final PropertyListener<T> listener, Executor executor,final Optional<T> oldValue,final T newValue) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				synchronized (listeners) {
					if(!listeners.containsKey(listener)) {
						return;
					}
				}
				listener.changed(oldValue, newValue);
			}
		});
	}
	
	private static final class MappedProperty<T,S> extends RWProperty<S> {
		@SuppressWarnings("unused")
		private final Property<T> parent;
		private final Function<T, S> mapper;
		private final PropertyListener<T> parentListener = new PropertyListener<T>() {
			@Override
			public void changed(Optional<T> oldValue, T newValue) {
				set(mapper.apply(newValue));
			}
		};
		
		public static <T,S> Property<S> create(Property<T> parent, Function<T, S> mapper) {
			MappedProperty<T,S> mapped = new MappedProperty<>(parent, mapper);
			parent.addListener(mapped.parentListener, new Immediate());
			return mapped;
		}
		
		private MappedProperty(Property<T> parent, Function<T, S> mapper) {
			super(mapper.apply(parent.get()));
			this.mapper = mapper;
			this.parent = parent;
		}
	}
}
