package fi.poo.hwo;

import com.google.common.base.Optional;

import fi.poo.hwo.ai.ActionPlan;
import fi.poo.hwo.connection.MsgSender;
import fi.poo.hwo.tools.Actor;
import fi.poo.hwo.tools.Property;
import fi.poo.hwo.tools.Property.PropertyListener;

public final class Driver extends Actor {

	private static final double RELEVANT_THROTTLE_DIFF_MULTIPLIER = 0.2;

	private final MsgSender sender;
	private final Property<Tick> gameTime;
	private final Property<CarState> carState;
	private final Property<OwnCar> ownCar;
	private final PropertyListener<Tick> timeListener = new PropertyListener<Tick>() {
		@Override
		public void changed(Optional<Tick> oldValue, Tick newValue) {
			triggerNextCommand();
		}
	};
	private final PropertyListener<CarState> carStateListener = new PropertyListener<CarState>() {
		@Override
		public void changed(Optional<CarState> oldValue, CarState newValue) {
			if (oldValue.isPresent() && newValue.getStartLane() != oldValue.get().getStartLane()) {
				laneChangeDone();
			}
		}
	};
	private final PropertyListener<OwnCar> ownCarListener = new PropertyListener<OwnCar>() {
		@Override
		public void changed(Optional<OwnCar> oldValue, OwnCar newValue) {
			ownCarState = newValue;
		}
	};

	private Optional<Double> requestedThrottle = Optional.absent();
	private Optional<Double> desiredThrottle = Optional.absent();
	private Optional<Direction> requestedDirection = Optional.absent();
	private Optional<Direction> desiredDirection = Optional.absent();
	private boolean desiredTurbo = false;
	private OwnCar ownCarState;

	public static Driver create(MsgSender sender, World world) {
		Driver driver = new Driver(sender, world);
		driver.gameTime.addListener(driver.timeListener, driver.getExecutor());
		driver.carState.addListener(driver.carStateListener, driver.getExecutor());
		driver.ownCar.addListener(driver.ownCarListener, driver.getExecutor());
		return driver;
	}

	private Driver(MsgSender sender, World world) {
		this.sender = sender;
		this.gameTime = world.currentTick();
		this.carState = world.getOwnCarState();
		this.ownCar = world.getOwnCar();
	}

	public void implement(final ActionPlan actionPlan) {
		submit(new Runnable() {
			@Override
			public void run() {
				desiredThrottle = Optional.of(actionPlan.throtte);
				desiredDirection = actionPlan.laneChange;
				desiredTurbo = actionPlan.useTurbo;
			}
		});
	}

	public void setThrottle(final double throttle) {
		if(!Double.isNaN(throttle)) {
			submit(new Runnable() {
				@Override
				public void run() {
					desiredThrottle = Optional.of(throttle);
				}
			});
		}
		else {
			new IllegalStateException("Who the F*CK is giving us NaNs?!?!?").printStackTrace();
		}
	}

	public void changeLane(final Optional<Direction> direction) {
		// TODO: Should we ignore requests if we are currently changing lanes?
		submit(new Runnable() {
			@Override
			public void run() {
				desiredDirection = direction;
			}
		});
	}

	public void enableTurbo() {
		submit(new Runnable() {
			@Override
			public void run() {
				desiredTurbo = true;
			}
		});
	}

	private void laneChangeDone() {
		if (requestedDirection.isPresent()) {
			if (desiredDirection.equals(requestedDirection)) {
				desiredDirection = Optional.absent();
			}
			requestedDirection = Optional.absent();
		}
	}

	private void triggerNextCommand() {
		if(needForSpeed()) {
			System.out.println(" [] Action; Priority throttle " + desiredThrottle.get());
			doThrottle();
		} else if (needForSteering()) {
			System.out.println(" [] Action; Steering " + desiredDirection.get());
			doLaneChange();
		} else if (needForTurbo()) {
			System.out.println(" [] Action; Turbo");
			doTurbo();
		} else if (needForSpeedTweak()) {
			System.out.println(" [] Action; Tweak throttle " + desiredThrottle.get());
			doThrottle();
		} else {
			System.out.println(" [] Action: No action");
			sender.ping();
		}
	}

	private boolean needForSpeed() {
//		System.out.println("Driver.needForSpeed calculation. Own throttle = " + ownCarState.getThrottle() + " while desired is " + desiredThrottle);
		if(desiredThrottle.isPresent()) {
			if(!ownCarState.getThrottle().isPresent()) {
				return true;
			}
			double throttleDiff = ownCarState.getThrottle().get()-desiredThrottle.get();
			double compareValue = Math.max(Math.abs(ownCarState.getThrottle().get()), Math.abs(desiredThrottle.get()));
			boolean result = Math.abs(throttleDiff) > RELEVANT_THROTTLE_DIFF_MULTIPLIER*compareValue;
			return result;
		}
		return false;
	}

	private boolean needForSteering() {
		return desiredDirection.isPresent() && !requestedDirection.equals(desiredDirection);
	}

	private boolean needForTurbo() {
		return desiredTurbo && ownCarState.getTurbo().isPresent() && ownCarState.getTurbo().get().isUsable();
	}

	private boolean needForSpeedTweak() {
		return desiredThrottle.isPresent() && !desiredThrottle.equals(requestedThrottle);
	}

	private void doThrottle() {
		sender.throttle(desiredThrottle.get());
		requestedThrottle = desiredThrottle;
	}

	private void doLaneChange() {
		if (desiredDirection.get().equals(Direction.LEFT)) {
			sender.changeLaneLeft();
		} else if (desiredDirection.get().equals(Direction.RIGHT)) {
			sender.changeLaneRight();
		}
		requestedDirection = desiredDirection;
	}

	private void doTurbo() {
		sender.turbo();
		desiredTurbo = false;
	}
}
