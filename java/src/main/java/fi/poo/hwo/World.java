package fi.poo.hwo;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import fi.poo.hwo.Car.Id;
import fi.poo.hwo.protocol.CarPositions;
import fi.poo.hwo.protocol.CarPositions.CarStateJson;
import fi.poo.hwo.protocol.CarPositions.PiecePosition;
import fi.poo.hwo.tools.Actor;
import fi.poo.hwo.tools.Property;
import fi.poo.hwo.tools.RWProperty;

public class World extends Actor {

	private static final Function<WorldState, Tick> TICK = new Function<WorldState, Tick>() {
		@Override
		public Tick apply(WorldState worldState) {
			return worldState.getTick();
		}
	};
	private final Function<WorldState, CarState> OWN_CAR_STATE = new Function<WorldState, CarState>() {
		@Override
		public CarState apply(WorldState worldState) {
			return worldState.getOwnCarState();
		}
	};
	private final Function<WorldState, OwnCar> OWN_CAR = new Function<WorldState, OwnCar>() {
		@Override
		public OwnCar apply(WorldState worldState) {
			return worldState.getOwnCar();
		}
	};

	private final Function<WorldState, Optional<TurboState>> OWN_CAR_TURBO = new Function<WorldState, Optional<TurboState>>() {

		@Override
		public Optional<TurboState> apply(WorldState worldState) {
			return worldState.getOwnCar().getTurbo();
		}
	};
	private final Track track;
	private final List<Car> cars;
	private final RWProperty<WorldState> state;

	public World(Track track, List<Car> cars, Car.Id ownCarId, Session session) {

		this.track = track;
		this.cars = cars;
		this.state = new RWProperty<WorldState>(initialState(cars, track, session, ownCarId));
	}

	public Property<WorldState> getState() {
		return state;
	}

	public void setOwnCarThrottle(final double throttle) {
		submit(new Runnable() {
			@Override
			public void run() {
				WorldState prev = state.get();
				if(!prev.getOwnCarState().isCrashed()) {
					WorldState newState = new WorldState(
							prev.getCarStates(), 
							state.get().getOwnCar().setThrottle(throttle), 
							track,
							prev.getTick(), 
							prev.getSession(), 
							Optional.of(prev.copy()));
					state.set(newState);
				}
			}
		});
	}

	public Property<CarState> getOwnCarState() {
		return getState().map(OWN_CAR_STATE);
	}

	public Property<OwnCar> getOwnCar() {
		return getState().map(OWN_CAR);
	}

	public Property<Tick> currentTick() {
		return getState().map(TICK);
	}

	public Property<Optional<TurboState>> getOwnCarTurbo() {
		return getState().map(OWN_CAR_TURBO);
	}

	public Track getTrack() {
		return track;
	}

	public void updateCarPositions(final CarPositions carPositions, long timePoint) {
		submit(new Runnable() {
			@Override
			public void run() {
				List<CarState> newCarStates = getNewCarStates(carPositions);
				WorldState worldState = state.get();
				WorldState newState = new WorldState(newCarStates, 
						getOwnCarWithUpdatedTurbo(worldState.getOwnCar()),
						track, new Tick(carPositions.getGameTick()), worldState.getSession(), Optional.of(worldState
								.copy()));
				state.set(newState);
			}
		});
	}

	private List<CarState> getNewCarStates(CarPositions carPositions) {
		Tick tick = carPositions.getGameTick().isPresent() ? new Tick(carPositions.getGameTick().get()) : Tick.unStarted();

		List<CarState> carStates = new ArrayList<>();
		for (CarStateJson json : carPositions.getCarStates()) {
			Car car = getCar(json.getId());

			PiecePosition piecePosition = json.getPiecePosition();
			Piece currentPiece = getCurrentPiece(piecePosition);

			carStates.add(
					new CarState(
							Optional.of(state.get().getCar(car.getId())), 
							tick, 
							car,
							currentPiece, 
							piecePosition.getInPieceDistance(),
							piecePosition.getStartLane(), 
							piecePosition.getEndLane(),
							json.getAngle(), 
							state.get().getCar(car.getId()).isCrashed()));
		}
		return carStates;
	}

	private Car getCar(Id id) {
		for (Car car : cars) {
			if (car.is(id)) {
				return car;
			}
		}
		throw new IllegalStateException("Unkown car!");
	}

	private Piece getCurrentPiece(PiecePosition piecePosition) {
		for (Piece piece : track.getPieces()) {
			if (piece.getIndex() == piecePosition.getIndex()) {
				return piece;
			}
		}
		throw new IllegalArgumentException("No piece with index "
				+ piecePosition.getIndex());
	}

	public void setOwnTurboEnabled() {
		submit(new Runnable() {

			@Override
			public void run() {

				WorldState prev = state.get();
				Optional<TurboState> prevTurbo = getOwnCarTurbo().get();
				if(prevTurbo.isPresent()) {
					WorldState newState = new WorldState(prev.getCarStates(), state
							.get().getOwnCar().setTurbo(prevTurbo.get().tick()), track,
							prev.getTick(),prev.getSession(), Optional.of(prev.copy()));
					state.set(newState);
				} else {
					throw new IllegalStateException("Turbo is not available!");
				}
			}

		});
	}

	public void setTurboAvailable(final TurboState turbo) {
		submit(new Runnable() {

			@Override
			public void run() {
				WorldState prev = state.get();
				WorldState newState = new WorldState(prev.getCarStates(), state
						.get().getOwnCar().setTurbo(Optional.of(turbo)), track,
						prev.getTick(),prev.getSession(), Optional.of(prev.copy()));
				state.set(newState);
			}
		});
	}

	public void gameStart() {
		submit(new Runnable() {

			@Override
			public void run() {
				WorldState prev = state.get();
				WorldState newState = new WorldState(prev.getCarStates(),
						prev.getOwnCar(), track, new Tick(0),prev.getSession(), Optional.of(prev
								.copy()));
				state.set(newState);
			}
		});
	}

	public void gameEnd() {
		submit(new Runnable() {

			@Override
			public void run() {
				state.set(initialState(cars, track, state.get().getSession(), state.get().getOwnCar()
						.getOwnCarId()));
			}
		});
	}

	public void crashed(final Id car, final Tick tick) {
		submit(new Runnable() {

			@Override
			public void run() {
				List<CarState> newCarStates = getNewCrashState(car, tick, true);
				WorldState newState = new WorldState(newCarStates, state.get()
						.getOwnCar().setThrottle(0.0), track, tick,state.get().getSession(), Optional.of(state.get()
								.copy()));
				state.set(newState);
			}
		});
	}

	public void spawned(final Id car, final Tick tick) {
		submit(new Runnable() {

			@Override
			public void run() {
				OwnCar ownCar = state.get().getOwnCar();
				if (car.equals(ownCar.getOwnCarId())) {
					ownCar = ownCar.setThrottle(0.0);
				}
				List<CarState> newCarStates = getNewCrashState(car, tick, false);
				WorldState newState = new WorldState(newCarStates, ownCar,
						track, tick, state.get().getSession(), Optional.of(state.get().copy()));
				state.set(newState);
			}
		});
	}

	public void session(final Session session) {
		submit(new Runnable() {

			@Override
			public void run() {

				WorldState newState = initialState(cars, track, session, state.get().getOwnCar().getOwnCarId());
				state.set(newState);
			}
		});
	}

	private List<CarState> getNewCrashState(Id car, Tick tick, boolean newState) {
		List<CarState> states = new ArrayList<>();
		for (CarState carState : state.get().getCarStates()) {
			if (carState.getCar().getId().equals(car)) {
				states.add(carState.crashed(tick, newState));
			} else {
				states.add(carState);
			}
		}
		return states;
	}

	private WorldState initialState(List<Car> cars, Track track, Session session, Car.Id ownCarId) {
		return new WorldState(createEmptyCarStates(cars, track), new OwnCar(
				ownCarId, Optional.<Double> absent(),
				Optional.<TurboState> absent()), track, session.isQualifying() ? new Tick(0) : Tick.unStarted(), session,
						Optional.<WorldState> absent());
	}

	private List<CarState> createEmptyCarStates(List<Car> cars, Track track) {
		List<CarState> states = Lists.newArrayList();
		for (Car car : cars) {
			states.add(new CarState(Optional.<CarState> absent(), new Tick(0),
					car, track.getPieces().get(0), 0, 0, 0, 0, false));
		}
		return states;
	}


	private OwnCar getOwnCarWithUpdatedTurbo(OwnCar ownCar) {
		if(ownCar.getTurbo().isPresent() && !ownCar.getTurbo().get().isUsable()) {
			return new OwnCar(ownCar.getOwnCarId(), ownCar.getThrottle(), ownCar.getTurbo().get().tick());
		} else {
			return ownCar;
		}
	}


}
