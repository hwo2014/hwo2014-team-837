package fi.poo.hwo;

import java.io.ObjectInputStream.GetField;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

public  abstract class Session {
	
	public abstract boolean isQualifying();

	public abstract boolean isQuickRace();
	
	public abstract boolean isRace();

	public abstract Optional<Integer> getLaps();
	
	public abstract Optional<Integer> getMaxLapTimeMs();
	
	
	public static class Qualifying  extends Session {
		private final int duration;
		
		public Qualifying(int duration) {
			this.duration = duration;
		}
		
		public int getDuration() {
			return duration;
		}

		@Override
		public boolean isQualifying() {
			return true;
		}

		@Override
		public boolean isQuickRace() {
			return false;
		}

		@Override
		public boolean isRace() {
			return false;
		}

		@Override
		public Optional<Integer> getLaps() {
			return Optional.absent();
		}

		@Override
		public Optional<Integer> getMaxLapTimeMs() {
			return Optional.absent();
		}
	}
	
	public static abstract class ARace extends Session {

		private final int laps;

		private final int maxLapTimeMs;

		public ARace(int laps,int maxLapTimeMs) {
			this.laps = laps;
			this.maxLapTimeMs = maxLapTimeMs;
		}
		
		public Optional<Integer> getLaps() {
			return Optional.of(laps);
		}

		public Optional<Integer> getMaxLapTimeMs() {
			return Optional.of(maxLapTimeMs);
		}
	}
	
	public static final class QuickRace extends ARace {

		
		public QuickRace(int laps,int maxLapTimeMs) {
			super(laps,maxLapTimeMs);
		}

		@Override
		public boolean isQualifying() {
			return false;
		}

		@Override
		public boolean isQuickRace() {
			return true;
		}

		@Override
		public boolean isRace() {
			return false;
		}
	}
	
	public static final class Race extends ARace {

		public Race(int laps,int maxLapTimeMs) {
			super(laps,maxLapTimeMs);
		}

		@Override
		public boolean isQualifying() {
			return false;
		}

		@Override
		public boolean isQuickRace() {
			return false;
		}

		@Override
		public boolean isRace() {
			return true;
		}

	}
}
