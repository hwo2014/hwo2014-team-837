package fi.poo.hwo;

import com.google.common.base.Optional;

public class CarState {
	// Basic
	private final Car car;

	// Given
	private final Piece currentPiece;
	private final double inPieceDistance;
	private final int startLane;
	private final int endLane;
	private final double angle;
	private final boolean crashed;
	private final Tick tick;

	// Calculated
	private final double angularVelocity;
	private final double speed;

	public CarState(
			Optional<CarState> oldState,
			Tick tick,
			Car car,
			Piece currentPiece, 
			double inPieceDistance, 
			int startLane,
			int endLane, 
			double angle, 
			boolean crashed) {
		this.tick = tick;
		this.car = car;
		this.currentPiece = currentPiece;
		this.inPieceDistance = inPieceDistance;
		this.startLane = startLane;
		this.endLane = endLane;
		this.angle = angle;
		this.crashed = crashed;

		if (oldState.isPresent() && tick.minus(oldState.get().tick) > 0.0) {
			speed = calculateSpeed(tick, oldState.get(), currentPiece, inPieceDistance, startLane);
			angularVelocity = calculateAngularVelocityDegreesPerTick(tick, oldState.get(), currentPiece, inPieceDistance, startLane);
		}
		else if (oldState.isPresent()) {
			speed = oldState.get().speed;
			angularVelocity = oldState.get().angularVelocity;
		}
		else {
			speed = 0.0;
			angularVelocity = 0.0;
		}
	}
	
	public CarState(
			Tick tick, 
			Car car,	
			double speed, 
			double angularVelocity, 
			Piece currentPiece, 
			double inPieceDistance, 
			int startLane, 
			int endLane, 
			double angle,
			boolean crashed) {
		this.tick = tick;
		this.car = car;
		this.currentPiece = currentPiece;
		this.inPieceDistance = inPieceDistance;
		this.startLane = startLane;
		this.endLane = endLane;
		this.angle = angle;
		this.crashed = crashed;
		this.speed = speed;
		this.angularVelocity = angularVelocity;
	}
	
	

	private double calculateAngularVelocityDegreesPerTick(Tick tick, CarState oldState, Piece currentPiece, double inPieceDistance, int lane) {
		// Angular velocity is only defined was in corner and is in corner
		if (oldState.currentPiece.equals(currentPiece) && currentPiece.isCorner()) {
			double time = tick.minus(oldState.tick);
			if(time <= 0) return 0.0;
			
			double radius = currentPiece.getRadius(lane);
			double arc = inPieceDistance - oldState.inPieceDistance;

			double angle = (360 * arc) / (2 * Math.PI * radius);

			return Math.abs(angle)/time;
		} else {
			return 0;
		}
	}

	private double calculateSpeed(Tick tick, CarState oldState, Piece currentPiece, double inPieceDistance, int lane) {
		double time = tick.minus(oldState.tick);
		if(time <= 0) return 0.0;
		double length = 0.0;

		if (oldState.currentPiece.equals(currentPiece)) {
			length = inPieceDistance - oldState.inPieceDistance;
		} else {
			System.out.println(" --- Old piece: " + oldState.currentPiece + " @ " + oldState.inPieceDistance);
			System.out.println(" --- New piece: " + currentPiece + " @ " + inPieceDistance);
			System.out.println(" --- => distance = " + (oldState.currentPiece.getLength(lane) - oldState.inPieceDistance + inPieceDistance));
			length = oldState.currentPiece.getLength(oldState.startLane) - oldState.inPieceDistance + inPieceDistance;
		}

		return Math.abs(length)/time;
	}

	public String getColor() {
		return car.getColor();
	}

	public Piece getCurrentPiece() {
		return currentPiece;
	}

	public double getInPieceDistance() {
		return inPieceDistance;
	}

	public int getStartLane() {
		return startLane;
	}

	public int getEndLane() {
		return endLane;
	}

	public double getAngle() {
		return angle;
	}

	public Car getCar() {
		return car;
	}

	public boolean isCrashed() {
		return crashed;
	}

	public CarState crashed(Tick tick, boolean newState) {
		return new CarState(tick, car, speed, angularVelocity, currentPiece, inPieceDistance, startLane, endLane, angle, newState);
	}

	public double getAngularVelocity() {
		return angularVelocity;
	}

	public double getSpeed() {
		return speed;
	}
}
