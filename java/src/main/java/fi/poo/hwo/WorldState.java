package fi.poo.hwo;

import java.util.List;

import com.google.common.base.Optional;

public class WorldState {
	private final OwnCar ownCar;
	private final List<CarState> cars;
	private final Track track;
	private final Tick tick;
	private final Session session;
	private final Optional<WorldState> previous;

	public WorldState(List<CarState> cars, OwnCar ownCar, Track track,
			Tick tick, Session session, Optional<WorldState> previous) {
		this.ownCar = ownCar;
		this.cars = cars;
		this.track = track;
		this.tick = tick;
		this.session = session;
		this.previous = previous;

		logOwnCarState();
	}

	private void logOwnCarState() {
		CarState state = getOwnCarState();
		OwnCar ownCar = getOwnCar();
		System.out.println(tick
				+ " CAR throttle: " + ((ownCar.getThrottle().isPresent()) ? ownCar.getThrottle().get() : "n/a") 
				+ " speed: " + state.getSpeed()
				+ " piece: " + state.getCurrentPiece().getIndex() 
				+ " inPiece:" + state.getInPieceDistance() 
				+ " angle:" + state.getAngle()
				+ " crashed: " + state.isCrashed());
	}

	public List<CarState> getCarStates() {
		return cars;
	}

	public Track getTrack() {
		return track;

	}

	public Tick getTick() {
		return tick;
	}
	
	public Session getSession() {
		return session;
	}

	public CarState getCar(Car.Id id) {
		for (CarState carState : cars) {
			if (carState.getCar().is(id)) {
				return carState;
			}
		}
		throw new IllegalStateException("Unknown car " + id);
	}

	public CarState getOwnCarState() {
		return getCar(ownCar.getOwnCarId());
	}

	public OwnCar getOwnCar() {
		return ownCar;
	}

	public Optional<Double> getOwnCarThrottle() {
		return ownCar.getThrottle();
	}

	public Optional<WorldState> getPrevious() {
		return previous;
	}

	public WorldState copy() {
		return new WorldState(cars, ownCar, track, tick, session,
				Optional.<WorldState> absent());
	}

	public double getOwnDistanceTo(Piece piece) {
		CarState own = getOwnCarState();
		return track.getDistanceBetween(own.getCurrentPiece(), piece, own.getEndLane())
				+ (piece.getLength(own.getEndLane()) - own.getInPieceDistance());
	}

}
