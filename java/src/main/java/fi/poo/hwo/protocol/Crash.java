package fi.poo.hwo.protocol;

import com.google.gson.annotations.SerializedName;

import fi.poo.hwo.Car;

public class Crash {

	@SerializedName("data")
	private Car.Id car;

	@SerializedName("gameId")
	private String gameId;
	
	@SerializedName("gameTick")
	private Integer tick;
	
	public Car.Id getCar() {
		return car;
	}
	
	public String getGameId() {
		return gameId;
	}
	
	public Integer getTick() {
		return tick;
	}
}
