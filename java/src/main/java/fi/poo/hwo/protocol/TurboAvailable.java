package fi.poo.hwo.protocol;

import com.google.gson.annotations.SerializedName;


public class TurboAvailable {

	@SerializedName("msgType")
	private String msgType;
	
	@SerializedName("data")
	private Data data;
	
	private static class Data {
		@SerializedName("turboDurationMilliseconds")
		private Double durationMillis;
		
		@SerializedName("turboDurationTicks")
		private Integer durationTicks;
		
		@SerializedName("turboFactor")
		private Double factor;
		
	}

	public double getDurationMillis() {
		return data.durationMillis;
	}

	public int getDurationTicks() {
		return data.durationTicks;
	}

	public double getFactor() {
		return data.factor;
	}
}
