package fi.poo.hwo.protocol;

import com.google.gson.annotations.SerializedName;

import fi.poo.hwo.Car;

public class YourCar {

	@SerializedName("data")
	private Car.Id data;
	
	public Car.Id getId() {
		return data;
	}
}
