package fi.poo.hwo.protocol;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.LinkedTreeMap;

import fi.poo.hwo.Car;
import fi.poo.hwo.Lane;
import fi.poo.hwo.Piece;
import fi.poo.hwo.Track;

public class GameInit {
	@SerializedName("msgType")
	private String msgType;

	@SerializedName("data")
	private Data data;

	public GameInit() {
	}

	public Track getTrack() {
		TrackJson track = data.getRace().getTrack();
		List<Lane> lanes = parseLanes(track);
		List<Piece> pieces = parsePieces(track, lanes);
		return new Track(track.getId(), track.getName(), pieces, lanes);
	}

	public List<Car> getCars() {
		List<Car> cars = new ArrayList<>();
		for (CarJson car : data.getRace().getCars()) {
			Car.Dimension dimension = new Car.Dimension(car.getDimension()
					.getLength(), car.getDimension().getWidth());
			cars.add(new Car(car.getId(), dimension));
		}
		return cars;
	}

	public RaceSession getRaceSession() {
		return data.getRace().getRaceSession();
	}

	private List<Piece> parsePieces(TrackJson track, List<Lane> lanes) {
		List<PieceJson> jsonPieces = track.getPieces();

		List<Piece> pieces = new ArrayList<>();

		for (int i = 0; i < jsonPieces.size(); i++) {
			PieceJson piece = jsonPieces.get(i);
			Optional<Double> angle = Optional.fromNullable(piece.getAngle());
			boolean hasSwitch = Optional.fromNullable(piece.getHasSwitch()).or(false);
			
			if (angle.isPresent()) {
				double radius = piece.getRadius();
				double calculatedLength = Math.abs(angle.get()) / 360 * 2 * Math.PI * radius;
				pieces.add(new Piece.Corner(calculatedLength, angle.get(), radius, i, hasSwitch, lanes));
			} else {
				pieces.add(new Piece.Line(piece.getLength(), 0.0, i, hasSwitch));
			}
		}
		return pieces;
	}

	private List<Lane> parseLanes(TrackJson track) {
		List<LinkedTreeMap<Object, Object>> lanesMapList = track.getLanes();

		List<Lane> lanes = new ArrayList<>();
		for (LinkedTreeMap<Object, Object> laneMap : lanesMapList) {
			double distanceFromCenter = (double) laneMap.get("distanceFromCenter");
			int index = ((Double) laneMap.get("index")).intValue();
			lanes.add(new Lane(distanceFromCenter, index));
		}
		return lanes;
	}

	public static class Data {
		@SerializedName("race")
		private Race race;

		public Race getRace() {
			return race;
		}

	}

	public static class Race {
		@SerializedName("track")
		private TrackJson track;

		@SerializedName("cars")
		private List<CarJson> cars;

		@SerializedName("raceSession")
		private RaceSession raceSession;

		public TrackJson getTrack() {
			return track;
		}

		public List<CarJson> getCars() {
			return cars;
		}

		public RaceSession getRaceSession() {
			return raceSession;
		}

	}

	public static class TrackJson {
		@SerializedName("id")
		private String id;

		@SerializedName("name")
		private String name;

		@SerializedName("pieces")
		private List<PieceJson> pieces;

		@SerializedName("lanes")
		private List<LinkedTreeMap<Object, Object>> lanes;

		public String getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		public List<PieceJson> getPieces() {
			return pieces;
		}

		public List<LinkedTreeMap<Object, Object>> getLanes() {
			return lanes;
		}
	}

	public static class PieceJson {
		@SerializedName("length")
		private Double length;

		@SerializedName("switch")
		private Boolean hasSwitch;

		@SerializedName("radius")
		private Double radius;

		@SerializedName("angle")
		private Double angle;

		public Double getLength() {
			return length;
		}

		public Boolean getHasSwitch() {
			return hasSwitch;
		}

		public Double getRadius() {
			return radius;
		}

		public Double getAngle() {
			return angle;
		}
	}

	public static class CarJson {
		@SerializedName("id")
		private Car.Id id;

		@SerializedName("dimensions")
		private CarDimension dimension;

		public Car.Id getId() {
			return id;
		}

		public CarDimension getDimension() {
			return dimension;
		}
	}

	public static class CarDimension {
		@SerializedName("length")
		private Double length;

		@SerializedName("width")
		private Double width;

		@SerializedName("guideFlagPosition")
		private Double guideFlagPos;

		public Double getLength() {
			return length;
		}

		public Double getWidth() {
			return width;
		}

		public Double getGuideFlagPosition() {
			return guideFlagPos;
		}
	}

	public static class RaceSession {
		
		@SerializedName("durationMs")
		private Integer qualifyingDuration;
		
		@SerializedName("laps")
		private Integer laps;

		@SerializedName("maxLapTimeMs")
		private Integer maxLapTimeMs;

		@SerializedName("quickRace")
		private Boolean quickRace;

		public int getLaps() {
			return laps;
		}

		public int getMaxLapTimeMs() {
			return maxLapTimeMs;
		}

		public boolean isQuickRace() {
			return quickRace;
		}
		
		public boolean isQualifying() {
			return Optional.fromNullable(qualifyingDuration).isPresent();
		}
		
		public Optional<Integer> getQualifyingDuration() {
			return Optional.fromNullable(qualifyingDuration);
		}


	}

}
