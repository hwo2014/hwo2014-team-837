package fi.poo.hwo.protocol;

public class Turbo extends SendMsg {
	@Override
	protected String msgType() {
		return "turbo";
	}

	@Override
	protected Object msgData() {
		return "weee!";
	}
}
