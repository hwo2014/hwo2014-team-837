package fi.poo.hwo.protocol;

import com.google.gson.annotations.SerializedName;

import fi.poo.hwo.Car;

public class Disqualified {
	
	@SerializedName("data")
	private Data data;

	@SerializedName("gameId")
	private String gameId;
	
	@SerializedName("gameTick")
	private Integer tick;

	private class Data {
		@SerializedName("car")
		private Car.Id car;
		
		@SerializedName("reason")
		private String reason;
	}

	public Car.Id getCarId() {
		return data.car;
	}

	public String getReason() {
		return data.reason;
	}

	public String getGameId() {
		return gameId;
	}

	public Integer getGameTick() {
		return tick;
	}

}
