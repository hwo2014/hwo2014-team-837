package fi.poo.hwo.protocol;

import java.util.List;

import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;

import fi.poo.hwo.Car;



public class CarPositions {
	@SerializedName("msgType")
	private String msgType;
	
	@SerializedName("data")
	private List<CarStateJson> carStates;
	
	@SerializedName("gameId")
	private String gameId;
	
	@SerializedName("gameTick")
	private Integer gameTick;
	

	public List<CarStateJson> getCarStates() {
		return carStates;
	}	
	
	public String getGameId() {
		return gameId;
	}

	public Optional<Integer> getGameTick() {
		return Optional.fromNullable(gameTick);
	}
	
	public static class CarStateJson {
		@SerializedName("id")
		private Car.Id id;
		
		@SerializedName("angle")
		private Double angle;
		
		@SerializedName("piecePosition")
		private PiecePosition piecePosition;
		
		
		public Car.Id getId() {
			return id;
		}
		

		public double getAngle() {
			return angle;
		}

		public PiecePosition getPiecePosition() {
			return piecePosition;
		}
		
		
			
	}
	
	public static class PiecePosition {
		@SerializedName("pieceIndex")
		private Double index;
		
		@SerializedName("inPieceDistance")
		private Double inPieceDistance;
		
		@SerializedName("lane")
		private LaneChange laneChange;
		
		@SerializedName("lap")
		private Double lap;
		
		public int getIndex() {
			return index.intValue();
		}
		
		public double getInPieceDistance() {
			return inPieceDistance;
		}
		
		public int getStartLane() {
			return laneChange.getStartLineIndex();
		}

		public int getEndLane() {
			return laneChange.getEndLineIndex();
		}
		
		public int getLap() {
			return lap.intValue();
		}
	}
	
	public static class LaneChange {
		@SerializedName("startLaneIndex")
		private Double startLineIndex;
		
		@SerializedName("endLaneIndex")
		private Double endLineIndex;

		public int getStartLineIndex() {
			return startLineIndex.intValue();
		}

		public int getEndLineIndex() {
			return endLineIndex.intValue();
		}
		
	}



}
