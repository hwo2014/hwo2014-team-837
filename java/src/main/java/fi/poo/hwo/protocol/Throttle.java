package fi.poo.hwo.protocol;


public final class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		if(Double.isNaN(value)) throw new IllegalStateException("I won't take that shit!");
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}