package fi.poo.hwo.protocol;


public final class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}