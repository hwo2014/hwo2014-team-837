package fi.poo.hwo.protocol;

public class SwitchLane extends SendMsg {
	public final String direction;

	public SwitchLane(final String direction) {
		this.direction = direction;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}
