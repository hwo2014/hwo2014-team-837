package fi.poo.hwo;

import java.util.List;

public abstract class Piece {
	private final double length;
	private final int index;
	private final boolean hasSwitch;

	public Piece(double length, int index, boolean hasSwitch) {
		this.length = length;
		this.index = index;
		this.hasSwitch = hasSwitch;
	}

	public double getLength(int lane) {
		return length;
	}

	public boolean hasSwitch() {
		return hasSwitch;
	}

	public int getIndex() {
		return index;
	}

	@Override
	public String toString() {
		return "Piece [length=" + length + ", index=" + index + ", hasSwitch=" + hasSwitch + "]";
	}

	public abstract boolean isCorner();
	public abstract double getRadius(int laneIndex);



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (hasSwitch ? 1231 : 1237);
		result = prime * result + index;
		long temp;
		temp = Double.doubleToLongBits(length);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piece other = (Piece) obj;
		if (hasSwitch != other.hasSwitch)
			return false;
		if (index != other.index)
			return false;
		if (Double.doubleToLongBits(length) != Double
				.doubleToLongBits(other.length))
			return false;
		return true;
	}



	public static class Corner extends Piece {
		private final double angle;
		private final double radius;
		private final List<Lane> lanes;

		public Corner(double length, double angle, double radius, int index, boolean hasSwitch, List<Lane> lanes) {
			super(length, index, hasSwitch);
			this.radius = radius;
			this.angle = angle;
			this.lanes = lanes;
		}

		@Override
		public boolean isCorner() {
			return true;
		}

		@Override
		public double getRadius(int laneIndex) {
			if(laneIndex >= 0 && laneIndex < lanes.size()) {
				Lane lane = lanes.get(laneIndex);
				if(angle < 0) return radius + lane.getDistanceFromCenter();
				else return radius - lane.getDistanceFromCenter();
			}
			new IllegalStateException("F*'d up! Requested length at non-existing lane!").printStackTrace();
			return radius;
		}

		@Override
		public double getLength(int laneIndex) {
			return (getRadius(laneIndex)/radius)*super.getLength(laneIndex);
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			long temp;
			temp = Double.doubleToLongBits(radius);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (getClass() != obj.getClass())
				return false;
			Corner other = (Corner) obj;
			if (Double.doubleToLongBits(radius) != Double
					.doubleToLongBits(other.radius))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Corner [length=" + getLength(0) + ", index=" + getIndex() + ", hasSwitch=" + hasSwitch() + ", radius=" + radius + "]";
		}
	}

	public static class Line extends Piece {

		public Line(double length, double radius, int index, boolean hasSwitch) {
			super(length, index, hasSwitch);
		}

		@Override
		public boolean isCorner() {
			return false;
		}

		@Override
		public double getRadius(int laneIndex) {
			return 0;
		}

	}

}
