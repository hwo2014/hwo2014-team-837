package fi.poo.hwo;

import java.util.concurrent.Callable;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.ListenableFuture;

import fi.poo.hwo.Car.Id;
import fi.poo.hwo.ai.AI;
import fi.poo.hwo.connection.GameClient;
import fi.poo.hwo.connection.MsgReceiver;
import fi.poo.hwo.connection.MsgSender;
import fi.poo.hwo.protocol.CarPositions;
import fi.poo.hwo.protocol.Crash;
import fi.poo.hwo.protocol.GameInit;
import fi.poo.hwo.protocol.GameInit.RaceSession;
import fi.poo.hwo.protocol.TurboAvailable;
import fi.poo.hwo.rules.RuleTracker;
import fi.poo.hwo.tools.Actor;

public class GameBot extends Actor implements MsgReceiver {

	private final MsgSender sender;

	private Optional<Car.Id> ownCarId = Optional.absent();
	private Optional<World> world = Optional.absent();
	private Optional<RuleTracker> ruleTracker = Optional.absent();
	private Optional<Driver> driver = Optional.absent();
	private Optional<AI> ai = Optional.absent();

	public GameBot(String botName, String botKey, String host, int port) {
		GameClient gameClient = new GameClient(this, botName, botKey);
		sender = gameClient;

		gameClient.connect(host, port);
		gameClient.join();
	}

	@Override
	public void gameStarted(long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {

			@Override
			public void run(World param) {
				param.gameStart();
			}
		});
	}

	@Override
	public void gameEnded(long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {

			@Override
			public void run(World param) {
				//TODO: results?
				param.gameEnd();
			}
		});
	}

	@Override
	public void carPositionsReceived(final CarPositions carPositions,
			final long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {
			@Override
			public void run(World param) {
				param.updateCarPositions(carPositions, timePoint);
			}
		});
	}

	@Override
	public void ownThrottleChanged(final double throttle) {
		submitWithWorld(new ParamRunnable<World>() {
			@Override
			public void run(World param) {
				param.setOwnCarThrottle(throttle);
			}
		});
	}

	@Override
	public void turboEnabled() {
		submitWithWorld(new ParamRunnable<World>() {
			@Override
			public void run(World param) {
				param.setOwnTurboEnabled();
			}
		});
	}

	@Override
	public void ownCar(final Id id) {
		submit(new Runnable() {

			@Override
			public void run() {
				if (!world.isPresent()) {
					ownCarId = Optional.of(id);
				} else {
					throw new IllegalStateException(
							"World already initialized!");
				}
			}
		});
	}

	@Override
	public void gameInitialized(final GameInit initMsg, long timePoint) {
		submit(new Runnable() {

			@Override
			public void run() {
				if (ownCarId.isPresent()) {
					Session session = getSession(initMsg.getRaceSession());
					 if(!world.isPresent()) {
						world = Optional.of(new World(initMsg.getTrack(), initMsg
								.getCars(), ownCarId.get(), session));
						ruleTracker = Optional.of(RuleTracker.create(world.get()));
						driver = Optional.of(Driver.create(sender, world.get()));
						ai = Optional.of(AI.create(world.get(), ruleTracker.get(),
								driver.get()));
					 } else {
						 world.get().session(session);
					 }
				} else {
					throw new IllegalStateException("No own car received yet!");
				}
			}

		});
	}

	@Override
	public void turboAvailable(final TurboAvailable turboAvailable,
			long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {

			@Override
			public void run(World param) {
				Tick durationTicks = new Tick(turboAvailable.getDurationTicks());
				param.setTurboAvailable(new TurboState(turboAvailable
						.getFactor(), durationTicks, Optional.of(durationTicks)));
			}
		});
	}

	@Override
	public void crash(final Crash carMsg, long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {

			@Override
			public void run(World param) {
				param.crashed(carMsg.getCar(), new Tick(carMsg.getTick()));
			}
		});
	}

	@Override
	public void spawn(final Crash carMsg, long timePoint) {
		submitWithWorld(new ParamRunnable<World>() {

			@Override
			public void run(World param) {
				param.spawned(carMsg.getCar(), new Tick(carMsg.getTick()));
			}
		});
	}

	private Session getSession(RaceSession raceSession) {
		if(raceSession.isQualifying() && raceSession.getQualifyingDuration().isPresent()) {
			return new Session.Qualifying(raceSession.getQualifyingDuration().get());
		} else if(raceSession.isQuickRace()) {
			return new Session.QuickRace(raceSession.getLaps(), raceSession.getMaxLapTimeMs());
		} else {
			return new Session.Race(raceSession.getLaps(), raceSession.getMaxLapTimeMs());
		}
	}
	
	private ListenableFuture<?> submitWithWorld(ParamRunnable<World> callBack) {
		return submitWithParameter(world(), callBack);
	}

	private Callable<World> world() {
		return new Callable<World>() {
			@Override
			public World call() throws Exception {
				if (world.isPresent()) {
					return world.get();
				} else {
					throw new IllegalStateException("World not initialized yet!");
				}
			}
		};
	}
}
