package fi.poo.hwo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;

public class Track {
	private final String id;
	private final String name;
	private final List<Piece> pieces;
	private final Map<Integer,Lane> lanes;
	
	public Track(String id, String name,List<Piece> pieces, List<Lane> lanes) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = Maps.uniqueIndex(lanes, new Function<Lane, Integer>() {

			@Override
			public Integer apply(Lane lane) {
				return lane.getIndex();
			}
		});
	}
	
	public Optional<Piece> getNextCorner(Piece piece) {
		Piece current = getNextPiece(piece);
		Optional<Piece> found = Optional.absent();
		while(!current.equals(piece) && !found.isPresent()) {
			if(current.isCorner()) found = Optional.of(current);
			current = getNextPiece(current);
		}
		return found;
		
//		Iterator<Piece> iterator = (piece.getIndex() == pieces.size() - 1) ? pieces.iterator() : pieces.listIterator(piece.getIndex());
//		Optional<Piece> found = Optional.absent();
//		while(!found.isPresent()) {
//			Piece next = iterator.next();
//			if(next.equals(piece)) {
//				break;
//			} else if(next.isCorner()) {
//				found = Optional.of(next);
//				break;
//			}
//			else if(!iterator.hasNext()) {
//				iterator = pieces.iterator();
//			}
//		}
//		return found;
	}

	public List<Lane> getLanes() {
		List<Lane> laneCopy = new ArrayList<>(lanes.values());
		Collections.sort(laneCopy, new Comparator<Lane>() {

			@Override
			public int compare(Lane o1, Lane o2) {
				if(o1.getIndex() < o2.getIndex()) {
					return -1;
				} else if(o1.getIndex() > o2.getIndex()) {
					return 1;
				}
				return 0;
			}
		});
		return laneCopy;
	}

	public List<Piece> getPieces() {
		return pieces;
	}

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	/**
	 * @param start
	 * @param stop
	 * @return the distance between the two pieces. Double.MAX_VALUE, if the stop piece isn't reachable from the start piece. 0 if they are the same piece, or the stop piece follows the start piece immediately.
	 */
	public double getDistanceBetween(Piece start, Piece stop, int lane) {
		if(!pieceBelongsToTrack(start)) {
			return Double.MAX_VALUE;
		}
		
		double distance = 0.0;
		Piece currentPiece = start;
		while(!currentPiece.equals(stop)) {
			currentPiece = getNextPiece(currentPiece);
			if(currentPiece.equals(start)) {
				return Double.MAX_VALUE;
			}
			else if(!currentPiece.equals(stop)) {
				distance += currentPiece.getLength(lane);
			}
		}
		return distance;
	}
	
	/**
	 * Sanity-check method
	 */
	private boolean pieceBelongsToTrack(Piece piece) {
		return piece.getIndex() < pieces.size() && piece.getIndex() > 0 && pieces.get(piece.getIndex()).equals(piece);
	}

	private Piece getNextPiece(Piece piece) {
		if(piece.getIndex()+1 < pieces.size()) {
			return pieces.get(piece.getIndex() + 1);
		}
		else {
			return pieces.get(0); 
		}
	}
	
}
