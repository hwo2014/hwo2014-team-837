package fi.poo.hwo.ai;

import com.google.common.base.Optional;

import fi.poo.hwo.Direction;

public class ActionPlan {
	private final double value;
	public final double throtte;
	public final Optional<Direction> laneChange;
	public final boolean useTurbo;
	public final String name;
	
	public ActionPlan(String name, double value, double throtte, Optional<Direction> laneChange, boolean useTurbo) {
		ensureNotNaN(value);
		ensureNotNaN(throtte);
		this.name = name;
		this.value = value;
		this.throtte = throtte;
		this.laneChange = laneChange;
		this.useTurbo = useTurbo;
	}

	public boolean isBetterThan(Optional<ActionPlan> other) {
		return !other.isPresent() || other.get().value < this.value;
	}
	
	private void ensureNotNaN(double number) {
		if(Double.isNaN(number)) {
			throw new IllegalStateException("That's not a plan! That's not even a bloody number!");
		}
	}

	@Override
	public String toString() {
		return "ActionPlan [name=" + name + ", value=" + value + ", throtte=" + throtte
				+ ", laneChange=" + laneChange + ", useTurbo=" + useTurbo + "]";
	}
}
