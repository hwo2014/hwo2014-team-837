package fi.poo.hwo.ai;

import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import fi.poo.hwo.Driver;
import fi.poo.hwo.World;
import fi.poo.hwo.WorldState;
import fi.poo.hwo.rules.RuleSet;
import fi.poo.hwo.rules.RuleTracker;
import fi.poo.hwo.tools.Actor;
import fi.poo.hwo.tools.Property.PropertyListener;

public final class AI extends Actor {

	private final Driver driver;
	private final List<Tactic> tactics;
	private final PropertyListener<WorldState> worldListener = new PropertyListener<WorldState>() {
		@Override
		public void changed(Optional<WorldState> oldValue, WorldState newValue) {
			world = Optional.of(newValue);
			react();
		}
	};
	private final PropertyListener<RuleSet> rulesListener = new PropertyListener<RuleSet>() {
		@Override
		public void changed(Optional<RuleSet> oldValue, RuleSet newValue) {
			rules = Optional.of(newValue);
			react();
		}
	};

	private Optional<WorldState> world;
	private Optional<RuleSet> rules;

	public static AI create(World world, RuleTracker ruleTracker, Driver driver) {
		AI ai = new AI(driver, initializeTactics());
		world.getState().addListener(ai.worldListener , ai.getExecutor());
		ruleTracker.getRules().addListener(ai.rulesListener , ai.getExecutor());
		return ai;
	}

	private static List<Tactic> initializeTactics() {
		Builder<Tactic> builder = ImmutableList.<Tactic>builder();
		builder.add(new DriveFastFTW());
		return builder.build();
	}

	private AI(Driver driver, List<Tactic> tactics) {
		this.driver = driver;
		this.tactics = tactics;
	}

	private void react() {

		if(world.isPresent() && world.get().getTick().isStarted() && rules.isPresent()) {
			Optional<ActionPlan> plan = getBestPlan(world.get(), rules.get());
			if(plan.isPresent()) {
				System.out.println("Going with plan " + plan);
				driver.implement(plan.get());
			} else {
				System.err.println("Uh, oh... Couldn't figure what to do!");
			}
		}
	}

	private Optional<ActionPlan> getBestPlan(WorldState state, RuleSet rules) {
		Optional<ActionPlan> selected = Optional.absent();
		for(Tactic tactic : tactics) {
			ActionPlan newPlan = tactic.execute(state, rules);
			if(newPlan.isBetterThan(selected)) {
				selected = Optional.of(newPlan);
			}
		}
		return selected;
	}
}
