package fi.poo.hwo.ai;

import fi.poo.hwo.WorldState;
import fi.poo.hwo.rules.RuleSet;

public interface Tactic {
	ActionPlan execute(WorldState state, RuleSet rules);
}
