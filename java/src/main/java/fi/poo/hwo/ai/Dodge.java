package fi.poo.hwo.ai;

import java.util.Collection;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import fi.poo.hwo.Car;
import fi.poo.hwo.CarState;
import fi.poo.hwo.Direction;
import fi.poo.hwo.Piece;
import fi.poo.hwo.WorldState;
import fi.poo.hwo.rules.RuleSet;

public class Dodge implements Tactic {

	@Override
	public ActionPlan execute(WorldState state, RuleSet rules) {
		//TODO: dodge to other lane if possible and necessary
		double value = calculateValue(state);
		Optional<Direction> dodge = Optional.absent();
		return new ActionPlan(getClass().getSimpleName(), value, 0.0, dodge, false);
	}

	private double calculateValue(WorldState state) {
		double value = 0.0;
		final CarState ownCarState = state.getOwnCarState();
		Piece currentPiece = ownCarState.getCurrentPiece();
		double inPieceDistance = ownCarState.getInPieceDistance();
		if(!currentPiece.hasSwitch() || inPieceDistance > currentPiece.getLength(ownCarState.getStartLane()) / 2) {
			value -= 10.0;
		} else {
			Collection<CarState> otherCars = getOtherCars(state, state.getOwnCar().getOwnCarId());
			
			for(CarState otherCar : otherCars) {
				if(isOtherCarBehind(ownCarState, otherCar) ) {
					//TODO: check speed and old distance
					//TODO: check piece type (corner)
				}
			}
		}
		return value;
	}

	private boolean isOtherCarBehind(final CarState ownCarState,CarState otherCar) {
		return otherCar.getSpeed() > ownCarState.getSpeed() && otherCar.getCurrentPiece().getIndex() == ownCarState.getCurrentPiece().getIndex()
				&& otherCar.getInPieceDistance() - ownCarState.getInPieceDistance() < 0;
	}

	private Collection<CarState> getOtherCars(WorldState state,
			final Car.Id ownCar) {
		Collection<CarState> otherCars = Collections2.filter(state.getCarStates(), new Predicate<CarState>() {

			@Override
			public boolean apply(CarState input) {
				return !input.getCar().getId().equals(ownCar);
			}
		});
		return otherCars;
	}

}
