package fi.poo.hwo.ai;

import com.google.common.base.Optional;

import fi.poo.hwo.CarState;
import fi.poo.hwo.Direction;
import fi.poo.hwo.Piece;
import fi.poo.hwo.WorldState;
import fi.poo.hwo.rules.RuleSet;

public class DriveFastFTW implements Tactic {

//	private static final double SAFE_DRIFT_UPPER_CAP = 45.0;
//	private static final double SAFE_DRIFT_LOWER_CAP = 10.0;
	private static final Double DANGER_AVOIDANCE_MULTIPLIER = 0.9;
	private static final double CORNER_PREEMPTION_SLOW_DISTANCE_MULTIPLIER = 1.8;
	private static final double CORNER_PREEMPTION_BREAK_DISTANCE_MULTIPLIER = 1.2;

	@Override
	public ActionPlan execute(WorldState state, RuleSet rules) {
		CarState ownCar = state.getOwnCarState();

		System.out.println(" X Deducing next move:");
		Optional<Double> desiredSpeed = getDesiredSpeed(ownCar, state, rules);
		System.out.println("    x Desired speed overall = " + desiredSpeed);
		
		double throttle = 1.0;
		if(desiredSpeed.isPresent()) {
			double maintainingThrottle = rules.speed().getMaintainingThrottle(desiredSpeed.get());
			double multiplier = getThrottleMultiplier(desiredSpeed.get(), ownCar.getSpeed());
			throttle = capThrottle(multiplier * maintainingThrottle);
			System.out.println("    x Maint.Throttle=" + maintainingThrottle + ", Multiplier=" + multiplier + " => Throttle=" + throttle);
		}

		// TODO: Turbo
		ActionPlan plan = new ActionPlan(getClass().getSimpleName(), 0.5, throttle, Optional.<Direction> absent(), false);
		
//		System.out.println("Plan: " + plan);
		return plan;
	}

	private Optional<Double> getDesiredSpeed(CarState ownCar, WorldState world, RuleSet rules) {
		Optional<Double> currentPieceSpeed = getCurrentPieceSpeed(ownCar, rules);
		Optional<Double> preemptiveSpeed = getPreemptiveSpeed(ownCar, world, rules);
		Optional<Double> immediateCompesationSpeed = getImmediateCompansationSpeed(ownCar, rules);

		System.out.println("    x Speed currentPiec=" + currentPieceSpeed + ", preemptive=" + preemptiveSpeed + ", immediate=" + immediateCompesationSpeed);
		
		return min(currentPieceSpeed, preemptiveSpeed, immediateCompesationSpeed);
	}

	@SafeVarargs
	private final Optional<Double> min(Optional<Double> ... values) {
		Optional<Double> min = Optional.absent();
		for(Optional<Double> val : values) {
			if(val.isPresent()) {
				if(!min.isPresent() || val.get() < min.get()) {
					min = val;
				}
			}
		}
		return min;
	}

	private Optional<Double> getPreemptiveSpeed(CarState ownCar, WorldState world, RuleSet rules) {
		Piece currentPiece = ownCar.getCurrentPiece();
		Optional<Piece> nextCorner = world.getTrack().getNextCorner(currentPiece);
//		System.out.println("Next corner is piece " + nextCorner);
		if (nextCorner.isPresent() && nextCorner.get().getRadius(ownCar.getEndLane()) != 0.0) {
//			double nextCornerSpeed = rules.drifting().getMaxSpeedWithNoDrift(nextCorner.get().getRadius()).weighted(DRIFT_EXPLORATIVE_WEIGHT);
			Optional<Double> nextCornerSpeed = getInPieceSafeSpeed(nextCorner.get(), rules, ownCar.getEndLane());
			if(!nextCornerSpeed.isPresent()) return Optional.absent();
			double distanceToCorner = world.getOwnDistanceTo(nextCorner.get());
			double ownBreakDistance = rules.speed().getMinBreakDistance(ownCar.getSpeed(),nextCornerSpeed.get());
			System.out.println("    x Next corner speed=" + nextCornerSpeed.get() + ", distance=" + distanceToCorner + ", breakDistance=" + ownBreakDistance);
			// When we're going fast and close to the corner, adjust to corner's speed
			if (ownBreakDistance*CORNER_PREEMPTION_SLOW_DISTANCE_MULTIPLIER >= distanceToCorner) {
				return Optional.of(nextCornerSpeed.get());
			}
			if (ownBreakDistance*CORNER_PREEMPTION_BREAK_DISTANCE_MULTIPLIER >= distanceToCorner) {
				return Optional.of(0.0);
			}
			double maxBreakDistance = rules.speed().getMinBreakDistance(rules.speed().getMaxSpeed(), nextCornerSpeed.get());
			// When we're not going fast enough, but maximum speed would be too fast, estimate something in between
			if (maxBreakDistance > distanceToCorner) {
				// This should be in the range (0.0,1.0)
				double proportionalDiff = distanceToCorner / maxBreakDistance;
				// double speed means squared breaking distance -> the under 1.0
				// multiplier should be squared to compensate
				double speedMultiplier = proportionalDiff * proportionalDiff;
				if (speedMultiplier >= 0.0 && speedMultiplier <= 1.0) {
					double estimatedOptimum = rules.speed().getMaxSpeed() * speedMultiplier;
					return Optional.of(Math.max(estimatedOptimum, ownCar.getSpeed()));
				} else {
					System.err.println("Calculations are off! Speed multiplier is "
							+ speedMultiplier
							+ " when expected a value in (0.0,1.0)");
					return Optional.absent();
				}
			}
		}
		return Optional.absent();
	}

	private Optional<Double> getCurrentPieceSpeed(CarState ownCar, RuleSet rules) {
		Optional<Double> dangerous = getInPieceDangerousSpeed(ownCar.getCurrentPiece(), rules, ownCar.getEndLane());
		Optional<Double> safe = getInPieceSafeSpeed(ownCar.getCurrentPiece(), rules, ownCar.getEndLane());
		if(dangerous.isPresent()) {
			double emergencyBreak = dangerous.get() * DANGER_AVOIDANCE_MULTIPLIER;
			if(safe.isPresent() && safe.get() > emergencyBreak) {
				emergencyBreak = safe.get();
			}
			if(ownCar.getSpeed() > emergencyBreak) {
				return Optional.of(0.0);
			}
		};
		return getInPieceSafeSpeed(ownCar.getCurrentPiece(), rules, ownCar.getEndLane());
	}
	
	private Optional<Double> getImmediateCompansationSpeed(CarState ownCar, RuleSet rules) {
//			double aimedDrift = getSafeDriftTarget(rules.speed());//.drifting());
//			if(ownCar.getAngle() > aimedDrift) {
//				double multiplier = (aimedDrift/ownCar.getAngle());
//				if(multiplier < 0.75) return Optional.of(0.0);
//				return Optional.of(ownCar.getSpeed() * multiplier * multiplier);
//			}
		return Optional.absent();
	}
	
	private Optional<Double> getInPieceSafeSpeed(Piece piece, RuleSet rules, int lane) {
		double radius = piece.getRadius(lane);
		if (radius > 0.0) {
			return Optional.of(rules.speed().getMaxSafeSpeed(radius));
//			double aimedDrift = getSafeDriftTarget(rules.drifting());
//			System.out.println("Target drift = " + aimedDrift);
//			return Optional.of(rules.drifting().getMaxSpeedWithDriftUnder(aimedDrift, piece.getRadius()));
//			return Optional.of(rules.drifting().getMaxSpeedWithNoDrift(currentPiece.getRadius()).plus(rules.drifting().getMaxDriftWithoutCrash()).weighted(DRIFT_EXPLORATIVE_WEIGHT));
		} else {
			return Optional.absent();
		}
	}
	
	private Optional<Double> getInPieceDangerousSpeed(Piece piece, RuleSet rules, int lane) {
		double radius = piece.getRadius(lane);
		if (radius > 0.0) {
			return Optional.of(rules.speed().getMinDangerousSpeed(radius));
		} else {
			return Optional.absent();
		}
	}
	
//	private double getSafeDriftTarget(DriftRules rules) {
//		Range noCrash = rules.getMaxDriftWithoutCrash();
//		if(noCrash.max < DriftRules.DEFAULT_NO_CRASH_MAX) {
//			return capDriftTarget(noCrash.weighted(DRIFT_EXPLORATIVE_WEIGHT));
//		}
//		else {
//			return capDriftTarget(noCrash.min*DRIFT_EXPLORATIVE_MULTIPLIER);
//		}
//	}
	
//	private double capDriftTarget(double drift) {
//		return Math.max(SAFE_DRIFT_LOWER_CAP, Math.min(SAFE_DRIFT_UPPER_CAP, drift));
//	}
	
	/**
	 * @param desiredSpeed
	 * @param currentSpeed
	 * @return A value for multiplying the maintaining throttle to get to the
	 *         target speed faster
	 */
	private double getThrottleMultiplier(double desiredSpeed, double currentSpeed) {
		if(currentSpeed == 0.0) return Double.MAX_VALUE;
		double multiplier = desiredSpeed / currentSpeed;
		if(Double.isNaN(multiplier)) {
			if(currentSpeed > desiredSpeed) return 0.0;
			else return 10.0;
		}
		return Math.abs(multiplier);
	}

	private double capThrottle(double throttle) {
		if (throttle < 0.0) return 0.0;
		if (throttle > 1.0) return 1.0;
		return throttle;
	}
}
