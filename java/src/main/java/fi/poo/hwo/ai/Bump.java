package fi.poo.hwo.ai;

import com.google.common.base.Optional;

import fi.poo.hwo.Direction;
import fi.poo.hwo.WorldState;
import fi.poo.hwo.rules.RuleSet;

public class Bump implements Tactic {

	@Override
	public ActionPlan execute(WorldState state, RuleSet rules) {
		double value = 0.0;
		return new ActionPlan(getClass().getSimpleName(), value, 0.0, Optional.<Direction>absent(), true);
	}

}
