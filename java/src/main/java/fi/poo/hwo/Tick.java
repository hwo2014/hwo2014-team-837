package fi.poo.hwo;

import com.google.common.base.Optional;

public class Tick {
	public static Tick unStarted() {
		return new Tick(Optional.<Integer> absent());
	}

	private final Optional<Integer> no;

	public Tick(int no) {
		this(Optional.of(no));
	}

	public Tick(Optional<Integer> no) {
		this.no = no;
	}

	public int minus(Tick other) {
		if (no.equals(other.no)) {
			return 0;
		}
		if (!no.isPresent()) {
			return -1 * other.no.get();
		} else if (!other.no.isPresent()) {
			return no.get();
		}
		return no.get() - other.no.get();
	}

	public boolean isStarted() {
		return !equals(unStarted());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((no == null) ? 0 : no.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tick other = (Tick) obj;
		if (no == null) {
			if (other.no != null)
				return false;
		} else if (!no.equals(other.no))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return (no.isPresent()) ? no.get().toString() : "n/a";
	}

}
