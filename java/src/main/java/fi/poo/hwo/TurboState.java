package fi.poo.hwo;

import com.google.common.base.Optional;

public class TurboState {

	private final double factor;
	private final Tick duration;
	private final Optional<Tick> ticksLeft;

	public TurboState(double factor, Tick durationTicks, Optional<Tick> ticksLeft) {
		this.factor = factor;
		this.duration = durationTicks;
		this.ticksLeft = ticksLeft;
	}
	
	public double getFactor() {
		return factor;
	}
	
	public Tick getTicksLeft() {
		return duration;
	}
	
	public boolean isUsable() {
		return ticksLeft.isPresent() && ticksLeft.get().equals(duration);
	}

	public Optional<TurboState> tick() {
		if(ticksLeft.isPresent()) {
			int left = ticksLeft.get().minus(new Tick(1));
			if(left <= 0) {
				return Optional.absent();
			} else {
				return Optional.of(new TurboState(factor, duration, Optional.of(new Tick(left))));
			}
		} else {
			return Optional.absent();
		}
	}

}
