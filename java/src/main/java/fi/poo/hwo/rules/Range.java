package fi.poo.hwo.rules;



public final class Range {
	public final double min;
	public final double max;

	public Range(double min, double max) {
		this.min = min;
		this.max = max;
	}
	
	public Range plus(Double value) {
		return new Range(min+value, max+value);
	}
	
	public Range plus(Range range) {
		return new Range(min+range.min, max+range.max);
	}
	
	public Range multiplyBy(double value) {
		return new Range(min*value, max*value);
	}

	public Range extendTo(double value) {
		return new Range(Math.min(min, value), Math.max(max, value));
	}
	
	public Range atLeast(double value) {
		return new Range(Math.max(min, value), Math.max(max, value));
	}

	public Range atMost(double value) {
		return new Range(Math.min(min, value), Math.min(max, value));
	}

	public double avg() {
		return (min + max) / 2;
	}
	
	public double weighted(double weight) {
		if(weight < 0.0d) return min;
		if(weight > 1.0d) return max;
		return min + Math.max(0.0, (max-min)*weight);
	}

	@Override
	public String toString() {
		return "IN_[" + min + ", " + max + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(max);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(min);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Range other = (Range) obj;
		if (Double.doubleToLongBits(max) != Double.doubleToLongBits(other.max))
			return false;
		if (Double.doubleToLongBits(min) != Double.doubleToLongBits(other.min))
			return false;
		return true;
	}
}
