package fi.poo.hwo.rules;

import com.google.common.base.Optional;


public class WitnessedMax {
	public final double witnessed;
	public final Optional<Double> cap;

	public WitnessedMax(double witnessed) {
		this(witnessed, Optional.<Double>absent());
	}
	
	public WitnessedMax(double witnessed, Optional<Double> cap) {
		this.witnessed = witnessed;
		this.cap = cap;
	}

	public WitnessedMax extendTo(double value) {
		double capped = cap(value);
		if(capped > witnessed)  {
			return new WitnessedMax(capped, cap);
		}
		return this;
	}
	
	private double cap(double value) {
		if(cap.isPresent() && cap.get() < value) return cap.get();
		return value;
	}

	@Override
	public String toString() {
		return "AT_LEAST " + witnessed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cap == null) ? 0 : cap.hashCode());
		long temp;
		temp = Double.doubleToLongBits(witnessed);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WitnessedMax other = (WitnessedMax) obj;
		if (cap == null) {
			if (other.cap != null)
				return false;
		} else if (!cap.equals(other.cap))
			return false;
		if (Double.doubleToLongBits(witnessed) != Double
				.doubleToLongBits(other.witnessed))
			return false;
		return true;
	}
}
