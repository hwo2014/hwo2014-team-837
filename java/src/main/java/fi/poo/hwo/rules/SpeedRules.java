package fi.poo.hwo.rules;

import com.google.common.base.Optional;

public final class SpeedRules {

	private static final boolean EXTRAPOLATE_ACCELERATION_FROM_THROTTLE = false;

	private static final double SAFE_DRIFT_VALUE = 45;
	private static final double UNSAFE_DRIFT_DELTA = 8.0;
	private static final double DRIFT_DELTA_PREEMPTION_MULTIPLIER = 4.0;
	private static final double UNKNOWN_MAX_SPEED_THRESHOLD = 0.5;

	private static final double SAFE_ANGULAR_LOWER_BOUND = 0.1;
	private static final double MAX_SAFE_SPEED_MULTIPLIER = 0.1;

	private static final double DECELERATION_MULTIPLIER = 0.25;

	private static final double READJUST_MULTIPLIER = 0.98;
	
	private final WitnessedMax maxSpeed;
	private final WitnessedMax maxAcceleration;
	private final WitnessedMax maxDeceleration;
	private final WitnessedMax maxSafeAngular;
	private final WitnessedMin minDangerousAngular;

	public SpeedRules() {
		this(new WitnessedMax(0.0), new WitnessedMax(0.0), new WitnessedMax(0.0), new WitnessedMax(0.0), new WitnessedMin(45.0));
	}
	
	public SpeedRules(WitnessedMax maxSpeed, WitnessedMax maxAcceleration, WitnessedMax maxDeceleration, WitnessedMax maxSafeAngular, WitnessedMin minDangerousAngular) {
		this.maxSpeed = maxSpeed;
		this.maxAcceleration = maxAcceleration;
		this.maxDeceleration = maxDeceleration;
		this.maxSafeAngular = maxSafeAngular;
		this.minDangerousAngular = minDangerousAngular;
	}
	
	public double getMaxSafeSpeed(double radius) {
		return Math.max(MAX_SAFE_SPEED_MULTIPLIER*getMaxSpeed(), angleVelocityToSpeedAt(getMaxSafeAngular(),radius));
	}
	
	public double getMinDangerousSpeed(double radius) {
		return Math.max(getMaxSafeSpeed(radius), angleVelocityToSpeedAt(getMinDangerousAngular(),radius));
	}
	
	public double getMaxSafeAngular() {
		return Math.min(minDangerousAngular.witnessed, Math.max(maxSafeAngular.witnessed, getDefaultMaxSafeAngular()));
	}
	
	public double getMinDangerousAngular() {
		return minDangerousAngular.witnessed;
	}

	private double getDefaultMaxSafeAngular() {
		return Math.max(SAFE_ANGULAR_LOWER_BOUND, maxSafeAngular.witnessed);
	}

	public SpeedRules seenSpeed(double speed) {
		return setMaxSpeed(maxSpeed.extendTo(speed));
	}

	public SpeedRules seenAcceleration(double acceleration, Optional<Double> throttle) {
		if(acceleration > 0) {
			if(EXTRAPOLATE_ACCELERATION_FROM_THROTTLE && throttle.isPresent()) {
				return setMaxAcceleration(maxAcceleration.extendTo(acceleration/throttle.get()));
			}
			return setMaxAcceleration(maxAcceleration.extendTo(acceleration));
		}
		else if(acceleration < 0) {
			if(EXTRAPOLATE_ACCELERATION_FROM_THROTTLE && throttle.isPresent()) {
				return setMaxDeceleration(maxDeceleration.extendTo(Math.abs(acceleration)/throttle.get()));
			}
			return setMaxDeceleration(maxDeceleration.extendTo(Math.abs(acceleration)));
		}
		return this;
	}
	
	public SpeedRules seenAngular(double angular, double drift, double driftDelta) {
		if(angular > SAFE_ANGULAR_LOWER_BOUND) {
			System.out.println("*** Angular = " + angular + ", Drift = " + drift + ", DriftDelta = " + driftDelta);
//			if(driftDelta <= 0 && drift <= SAFE_DRIFT_VALUE) {
//				System.out.println("*** --> SAFE");
//				return setMaxSafeAngular(maxSafeAngular.extendTo(angular));
//			}
			if(SAFE_DRIFT_VALUE < (drift + driftDelta*DRIFT_DELTA_PREEMPTION_MULTIPLIER) || driftDelta > UNSAFE_DRIFT_DELTA) {
				System.out.println("*** --> DANGEROUS");
//				return setMinDangerousAngular(minDangerousAngular.extendTo(angular));
				if(angular < minDangerousAngular.witnessed) {
					if(maxSafeAngular.witnessed > angular) {
						double avg = angular + READJUST_MULTIPLIER*(maxSafeAngular.witnessed - angular);
						return setMinDangerousAngular(new WitnessedMin(avg)).setMaxSafeAngular(new WitnessedMax(avg));
					}
					else {
						return setMinDangerousAngular(minDangerousAngular.extendTo(Math.max(angular,maxSafeAngular.witnessed)));
					}
				}
			}
			else if(driftDelta <= 0){
				System.out.println("*** --> SAFE");
				if(angular > maxSafeAngular.witnessed) {
					if(minDangerousAngular.witnessed < angular) {
						double avg = minDangerousAngular.witnessed + (1.0-READJUST_MULTIPLIER)*(angular-minDangerousAngular.witnessed);
						return setMinDangerousAngular(new WitnessedMin(avg)).setMaxSafeAngular(new WitnessedMax(avg));
					}
					else {
						return setMaxSafeAngular(maxSafeAngular.extendTo(angular));
					}
				}
			}
			else {
				System.out.println("*** --> UNDECIDED");
			}
		}
		return this;
	}

	/**
	 * @param startSpeed
	 * @param acceleration
	 * @return The best estimate of a throttle that yields the given
	 *         acceleration from the given starting speed
	 */
	public double getThrottle(double startSpeed, double acceleration) {
		double windFactor = (startSpeed/maxSpeed.witnessed)*maxAcceleration.witnessed;
		double totalAccel;
		if(acceleration >= 0.0) {
			totalAccel = acceleration + windFactor;
		}
		else {
			totalAccel = acceleration - windFactor;
		}
		if(totalAccel <= 0.0) return 0.0;
		if(totalAccel >= maxAcceleration.witnessed) return 1.0;
		return totalAccel/maxAcceleration.witnessed;
	}

	/**
	 * @param speed
	 * @return The best estimate of a throttle that is required to maintain the
	 *         given speed
	 */
	public double getMaintainingThrottle(double speed) {
		if(maxSpeed.witnessed <= UNKNOWN_MAX_SPEED_THRESHOLD) return 0.5;
		return speed/maxSpeed.witnessed;
	}

	/**
	 * @param startSpeed
	 * @param targetSpeed
	 *            Must be lower than the startSpeed
	 * @return The best estimate of a distance required to break from the given
	 *         starting speed to the given target speed.
	 */
	public double getMinBreakDistance(double startSpeed, double targetSpeed) {
		assert targetSpeed < startSpeed;
		double avg = (targetSpeed+startSpeed)/2;
		double delta = Math.abs(targetSpeed-startSpeed);
		double deceleration = getMaxDeceleration()*((targetSpeed*0.5/getMaxSpeed())+0.5);
		double time = delta/deceleration;
		return Math.abs(avg*time);
	}
	
	private double getMaxDeceleration() {
		double guess = (Math.max(maxAcceleration.witnessed, 1.0)*DECELERATION_MULTIPLIER);
		return Math.max(maxDeceleration.witnessed, guess);
	}

	public double getMaxSpeed() {
		return maxSpeed.witnessed;
	}

	private SpeedRules setMaxSpeed(WitnessedMax newMax) {
		if(newMax.equals(maxSpeed)) return this;
		return new SpeedRules(newMax, maxAcceleration, maxDeceleration, maxSafeAngular, minDangerousAngular);
	}

	private SpeedRules setMaxAcceleration(WitnessedMax newMax) {
		if(newMax.equals(maxAcceleration)) return this;
		return new SpeedRules(maxSpeed, newMax, maxDeceleration, maxSafeAngular, minDangerousAngular);
	}

	private SpeedRules setMaxDeceleration(WitnessedMax newMax) {
		if(newMax.equals(maxDeceleration)) return this;
		return new SpeedRules(maxSpeed, maxAcceleration, newMax, maxSafeAngular, minDangerousAngular);
	}

	private SpeedRules setMaxSafeAngular(WitnessedMax newMax) {
		if(newMax.equals(maxSafeAngular)) return this;
		return new SpeedRules(maxSpeed, maxAcceleration, maxDeceleration, newMax, minDangerousAngular);
	}
	
	private SpeedRules setMinDangerousAngular(WitnessedMin newMin) {
		if(newMin.equals(minDangerousAngular)) return this;
		return new SpeedRules(maxSpeed, maxAcceleration, maxDeceleration, maxSafeAngular, newMin);
	}
	
	private double angleVelocityToSpeedAt(double angleVelocity, double cornerRadius) {
		return angleVelocity*((2*Math.PI*cornerRadius) / 360);
	}
	
	@Override
	public String toString() {
		return "Speed [maxSpd=" + maxSpeed + ", maxAccel=" + maxAcceleration + ", maxDecel=" + maxDeceleration + ", maxSafeAng=" + maxSafeAngular + ", minDangerAng=" + minDangerousAngular + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((maxAcceleration == null) ? 0 : maxAcceleration.hashCode());
		result = prime * result
				+ ((maxDeceleration == null) ? 0 : maxDeceleration.hashCode());
		result = prime * result
				+ ((maxSafeAngular == null) ? 0 : maxSafeAngular.hashCode());
		result = prime * result
				+ ((maxSpeed == null) ? 0 : maxSpeed.hashCode());
		result = prime
				* result
				+ ((minDangerousAngular == null) ? 0 : minDangerousAngular
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SpeedRules other = (SpeedRules) obj;
		if (maxAcceleration == null) {
			if (other.maxAcceleration != null)
				return false;
		} else if (!maxAcceleration.equals(other.maxAcceleration))
			return false;
		if (maxDeceleration == null) {
			if (other.maxDeceleration != null)
				return false;
		} else if (!maxDeceleration.equals(other.maxDeceleration))
			return false;
		if (maxSafeAngular == null) {
			if (other.maxSafeAngular != null)
				return false;
		} else if (!maxSafeAngular.equals(other.maxSafeAngular))
			return false;
		if (maxSpeed == null) {
			if (other.maxSpeed != null)
				return false;
		} else if (!maxSpeed.equals(other.maxSpeed))
			return false;
		if (minDangerousAngular == null) {
			if (other.minDangerousAngular != null)
				return false;
		} else if (!minDangerousAngular.equals(other.minDangerousAngular))
			return false;
		return true;
	}
	
}
