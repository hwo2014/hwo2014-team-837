package fi.poo.hwo.rules;

import java.util.Map;
import java.util.Set;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import fi.poo.hwo.CarState;
import fi.poo.hwo.OwnCar;
import fi.poo.hwo.Tick;
import fi.poo.hwo.World;
import fi.poo.hwo.WorldState;
import fi.poo.hwo.tools.Actor;
import fi.poo.hwo.tools.Property;
import fi.poo.hwo.tools.Property.PropertyListener;
import fi.poo.hwo.tools.RWProperty;

public final class RuleTracker extends Actor {

	private final RWProperty<RuleSet> rules = new RWProperty<>(new RuleSet());
	private final PropertyListener<WorldState> worldListener = new PropertyListener<WorldState>() {
		@Override
		public void changed(Optional<WorldState> oldValue, WorldState newValue) {
			updateRules(oldValue, newValue);
		}
	};

	private Map<String, CarState> previousCarStates;
	private Optional<Tick> previousTick = Optional.absent();

	public static RuleTracker create(World world) {
		RuleTracker tracker = new RuleTracker();
		world.getState().addListener(tracker.worldListener, tracker.getExecutor());
		return tracker;
	}

	private RuleTracker() {
	}

	public Property<RuleSet> getRules() {
		return rules;
	}

	private void updateRules(Optional<WorldState> oldWorldState, WorldState worldState) {
		Set<CarParameters> carParameters = Sets.newHashSet();
		Map<String, CarState> carStates = getCarStates(worldState);
		if (previousTick.isPresent() && !previousTick.get().equals(worldState.getTick()) && worldState.getTick().isStarted()) {
			int delta = worldState.getTick().minus(previousTick.get());

			if(delta > 0) {
				for (CarStatePair carPair : getCarStates(previousCarStates, carStates)) {
					if(oldWorldState.isPresent()) {
						// Use old own car state, since it's used to calculate the effects of the actions of that time
						carParameters.add(calculateCarParameters(delta, carPair, Optional.of(oldWorldState.get().getOwnCar())));
					}
					else {
						carParameters.add(calculateCarParameters(delta, carPair, Optional.<OwnCar>absent()));
					}
				}
			}
		}

		previousTick = Optional.of(worldState.getTick());
		previousCarStates = carStates;

		updateRuleSet(carParameters);
	}

	private void updateRuleSet(Set<CarParameters> carParameters) {
		RuleSet oldRules = rules.get();

		SpeedRules speedRules = getSpeedRules(oldRules.speed(), carParameters);
//		DriftRules driftRules = getDriftRules(oldRules.drifting(), carParameters);

		rules.set(oldRules.setSpeedRules(speedRules));//.setDriftRules(driftRules));
	}

	private SpeedRules getSpeedRules(SpeedRules oldRules, Set<CarParameters> carParameters) {
		SpeedRules result = oldRules;

		for (CarParameters carParameter : carParameters) {
			result = update(result, carParameter);
		}

		return result;
	}

	private SpeedRules update(SpeedRules old, CarParameters carParameter) {
		if(!carParameter.isCrashed && !carParameter.wasCrashed) {
			return old.seenSpeed(carParameter.speed).seenAcceleration(carParameter.acceleration, carParameter.throttle).seenAngular(carParameter.angularVelocity, carParameter.drift, carParameter.driftDelta);
		}
		return old;
	}

	private DriftRules getDriftRules(DriftRules oldRules, Set<CarParameters> carParameters) {
		DriftRules result = oldRules;

		for (CarParameters carParameter : carParameters) {
			result = update(result, carParameter);
		}

		return result;
	}

	private DriftRules update(DriftRules old, CarParameters car) {
		DriftRules result = old;

		if(car.isCrashed && !car.wasCrashed) {
			result = result.crashedWithDriftAt(car.drift);
		} else if(!car.isCrashed && !car.wasCrashed) {
			System.out.println("Car drifting at " + car.angularVelocity + "=>" + car.drift);
			result = result.didntCrashWithDriftAt(car.drift).seenDrifting(car.angularVelocity, car.drift);
		}

		return result;
	}

	private CarParameters calculateCarParameters(int tickDelta, CarStatePair carPair, Optional<OwnCar> ownCar) {
		double speed = carPair.current.getSpeed();
		double acceleration = (carPair.current.getSpeed() - carPair.previous.getSpeed())/tickDelta;
		double angularVelocity = carPair.current.getAngularVelocity();
		double drift = Math.abs(carPair.current.getAngle());
		double driftDelta = Math.abs(carPair.current.getAngle()) - Math.abs(carPair.previous.getAngle());
		Optional<Double> throttle = Optional.absent();
		if(ownCar.isPresent() && carPair.current.getCar().is(ownCar.get().getOwnCarId())) {
			throttle = ownCar.get().getThrottle();
		}

		return new CarParameters(speed, acceleration, angularVelocity, drift, driftDelta, carPair.current.isCrashed(), carPair.previous.isCrashed(), throttle);
	}

	private Set<CarStatePair> getCarStates(
			Map<String, CarState> previousCarStates,
			Map<String, CarState> currentCarStates) {
		Set<CarStatePair> carStates = Sets.newHashSet();
		for (String key : previousCarStates.keySet()) {
			carStates.add(new CarStatePair(previousCarStates.get(key),
					currentCarStates.get(key)));
		}
		return carStates;
	}

	private Map<String, CarState> getCarStates(WorldState worldState) {
		Map<String, CarState> carStates = Maps.newHashMap();
		for (CarState car : worldState.getCarStates()) {
			carStates.put(car.getColor(), car);
		}
		return carStates;
	}

	private class CarStatePair {
		public final CarState previous;
		public final CarState current;

		public CarStatePair(CarState previous, CarState current) {
			this.previous = previous;
			this.current = current;
		}
	}

	private class CarParameters {
		private final double speed;
		private final double angularVelocity;
		private final double drift;
		private final double driftDelta;
		private final boolean isCrashed;
		private final boolean wasCrashed;
		private final double acceleration;
		private final Optional<Double> throttle;

		public CarParameters(double speed, double acceleration, double angularVelocity, double drift, double driftDelta, boolean isCrashed, boolean wasCrashed, Optional<Double> throttle) {
			this.speed = speed;
			this.acceleration = acceleration;
			this.throttle = throttle;
			this.angularVelocity = angularVelocity;
			this.drift = drift;
			this.driftDelta = driftDelta;
			this.isCrashed = isCrashed;
			this.wasCrashed = wasCrashed;
		}
	}
}
