package fi.poo.hwo.rules;



public class RuleSet {
	private final SpeedRules speedRules;
//	private final DriftRules driftRules;

	public RuleSet(SpeedRules speedRules) {//, DriftRules driftRules) {
		this.speedRules = speedRules;
//		this.driftRules = driftRules;

		System.out.println(this);
	}

	public RuleSet() {
		this.speedRules = new SpeedRules();
//		this.driftRules = new DriftRules();
	}

	public RuleSet setSpeedRules(SpeedRules newSpeedRules) {
		System.out.println(" O Setting new speedrules:");
		System.out.println("     o Old: " + speedRules);
		System.out.println("     o New: " + newSpeedRules);
		
		if(speedRules.equals(newSpeedRules)) {
			return this;
		}
		return new RuleSet(newSpeedRules);//, driftRules);
	}

//	public RuleSet setDriftRules(DriftRules newDriftRules) {
//		if(driftRules.equals(newDriftRules)) {
//			return this;
//		}
//		return new RuleSet(speedRules, newDriftRules);
//	}
	
	public SpeedRules speed() {
		return speedRules;
	}
	
//	public DriftRules drifting() {
//		return driftRules;
//	}
	
	@Override
	public String toString() {
		return "Rules [" + speedRules + /*", " + driftRules +*/ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((speedRules == null) ? 0 : speedRules.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleSet other = (RuleSet) obj;
		if (speedRules == null) {
			if (other.speedRules != null)
				return false;
		} else if (!speedRules.equals(other.speedRules))
			return false;
		return true;
	}
	
}
