package fi.poo.hwo.rules;

import java.util.Collections;
import java.util.List;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

public final class LivingAverage {
	private final int maxItems;
	private final List<Double> values;
	private final Optional<Double> avg;
	
	public LivingAverage(int maxItems) {
		this(maxItems, Collections.<Double>emptyList());
	}
	
	private LivingAverage(int maxItems, List<Double> values) {
		this.maxItems = maxItems;
		this.values = values;
		this.avg = calculate();
	}
	
	public Optional<Double> get() {
		return avg;
	}
	
	public LivingAverage add(double value) {
		Builder<Double> builder = ImmutableList.<Double>builder();
		if(values.size() < maxItems) {
			builder.addAll(values);
		}
		else {
			builder.addAll(values.subList(1, values.size()));
		}
		builder.add(value);
		return new LivingAverage(maxItems, builder.build());
	}
	
	private Optional<Double> calculate() {
		if(values.isEmpty()) {
			return Optional.absent();
		}
		double total = 0.0;
		for(Double value : values) {
			total += value;
		}
		return Optional.of(total/values.size());
	}

	@Override
	public String toString() {
		return avg.isPresent() ? "<~" + avg.get() + ">" : "<Unknown>";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avg == null) ? 0 : avg.hashCode());
		result = prime * result + maxItems;
		result = prime * result + ((values == null) ? 0 : values.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LivingAverage other = (LivingAverage) obj;
		if (avg == null) {
			if (other.avg != null)
				return false;
		} else if (!avg.equals(other.avg))
			return false;
		if (maxItems != other.maxItems)
			return false;
		if (values == null) {
			if (other.values != null)
				return false;
		} else if (!values.equals(other.values))
			return false;
		return true;
	}
}
