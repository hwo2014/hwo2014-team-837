package fi.poo.hwo.rules;



public class DriftRules {

	public static final double DEFAULT_NO_CRASH_MAX = 90.0;
	public static final double DEFAULT_MAX_ANGULAR_VELOCITY_WITHOUT_DRIFT = 180.0;
	
	private static final int MULTIPLIER_CALCULATION_ITEMS = 5;
	private static final double DRIFT_NEGLECT_LIMIT = 0.01;
	private static final double ANGLE_VELOCITY_NEGLECT_LIMIT = 0.001;
	
	private final Range maxAngularVelocityWithoutDrift;
	private final Range maxDriftWithoutCrash;
	private final LivingAverage driftMultiplier;
	
	public DriftRules(Range maxAngularVelocityWithoutDrift, Range maxDriftWithoutCrash, LivingAverage driftMultiplier) {
		this.maxAngularVelocityWithoutDrift = maxAngularVelocityWithoutDrift;
		this.maxDriftWithoutCrash = maxDriftWithoutCrash;
		this.driftMultiplier = driftMultiplier;
	}

	public DriftRules() {
		this(new Range(0, DEFAULT_MAX_ANGULAR_VELOCITY_WITHOUT_DRIFT),new Range(0,DEFAULT_NO_CRASH_MAX), new LivingAverage(MULTIPLIER_CALCULATION_ITEMS).add(0.1).add(0.1).add(0.1));
	}

	public DriftRules crashedWithDriftAt(double drift) {
		return setMaxDriftWithoutCrash(maxDriftWithoutCrash.atMost(Math.abs(drift)));
	}

	public DriftRules didntCrashWithDriftAt(double drift) {
		return setMaxDriftWithoutCrash(maxDriftWithoutCrash.atLeast(Math.abs(drift)));
	}

	public DriftRules seenDrifting(double angularVelocity, double drift) {
		double angVelAbs = Math.abs(angularVelocity);
		double driftAbs = Math.abs(drift);
		DriftRules result = this;
		
		if(driftAbs > DRIFT_NEGLECT_LIMIT) {
			return result.setMaxAngulatVelocityWithoutDrift(maxAngularVelocityWithoutDrift.atMost(angVelAbs)).updateDriftMultiplier(angVelAbs, driftAbs);
		} else {
			return result.setMaxAngulatVelocityWithoutDrift(maxAngularVelocityWithoutDrift.atLeast(angVelAbs));
		}
	}

	public Range getMaxAngularVelocityWithoutDrift() {
		return maxAngularVelocityWithoutDrift;
	}

	public Range getMaxAngularVelocityWithDriftUnder(double drift) {
		if(driftMultiplier.get().isPresent()) {
			return maxAngularVelocityWithoutDrift.plus(drift*driftMultiplier.get().get());
		}
		return maxAngularVelocityWithoutDrift;
	}

	public Range getMaxDriftWithoutCrash() {
		return maxDriftWithoutCrash;
	}

	public Range getMaxSpeedWithNoDrift(double cornerRadius) {
		return angleVelocityToSpeedAt(getMaxAngularVelocityWithoutDrift(), cornerRadius);
	}

	/**
	 * @param driftAngle
	 * @param angle
	 * @return The best estimate of a speed that yields the given drift angle in
	 *         a corner with given radius
	 */
	public double getMaxSpeedWithDriftUnder(double driftAngle, double cornerRadius) {
		return angleVelocityToSpeedAt(getMaxAngularVelocityWithDriftUnder(driftAngle), Math.abs(cornerRadius)).min;
	}
	
	private Range angleVelocityToSpeedAt(Range angleVelocity, double cornerRadius) {
		return angleVelocity.multiplyBy((2*Math.PI*cornerRadius) / 360);
	}

	private DriftRules updateDriftMultiplier(double angularVelocity, double drift) {
		double excess = Math.abs(angularVelocity) - getMaxAngularVelocityWithoutDrift().min;
		if(drift <= DRIFT_NEGLECT_LIMIT || excess <= ANGLE_VELOCITY_NEGLECT_LIMIT) return this;
		return new DriftRules(maxAngularVelocityWithoutDrift, maxDriftWithoutCrash, driftMultiplier.add(excess/drift));
	}
	
	private DriftRules setMaxAngulatVelocityWithoutDrift(Range newMaxAngularVelocityWithoutDrift) {
		if(newMaxAngularVelocityWithoutDrift.equals(maxAngularVelocityWithoutDrift)) {
			return this;
		}
		return new DriftRules(newMaxAngularVelocityWithoutDrift, maxDriftWithoutCrash, driftMultiplier);
	}
	
	private DriftRules setMaxDriftWithoutCrash(Range newMaxDriftWithoutCrash) {
		if(newMaxDriftWithoutCrash.equals(maxDriftWithoutCrash)) {
			return this;
		}
		return new DriftRules(maxAngularVelocityWithoutDrift, newMaxDriftWithoutCrash, driftMultiplier);
	}

	@Override
	public String toString() {
		return "Drifting ["
				+ "maxAngularVelocityWithoutDrift=" + maxAngularVelocityWithoutDrift 
				+ ", maxDriftWithoutCrash=" + maxDriftWithoutCrash 
				+ ", driftMultiplier=" + driftMultiplier 
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((driftMultiplier == null) ? 0 : driftMultiplier.hashCode());
		result = prime
				* result
				+ ((maxAngularVelocityWithoutDrift == null) ? 0
						: maxAngularVelocityWithoutDrift.hashCode());
		result = prime
				* result
				+ ((maxDriftWithoutCrash == null) ? 0 : maxDriftWithoutCrash
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DriftRules other = (DriftRules) obj;
		if (driftMultiplier == null) {
			if (other.driftMultiplier != null)
				return false;
		} else if (!driftMultiplier.equals(other.driftMultiplier))
			return false;
		if (maxAngularVelocityWithoutDrift == null) {
			if (other.maxAngularVelocityWithoutDrift != null)
				return false;
		} else if (!maxAngularVelocityWithoutDrift
				.equals(other.maxAngularVelocityWithoutDrift))
			return false;
		if (maxDriftWithoutCrash == null) {
			if (other.maxDriftWithoutCrash != null)
				return false;
		} else if (!maxDriftWithoutCrash.equals(other.maxDriftWithoutCrash))
			return false;
		return true;
	}
}
