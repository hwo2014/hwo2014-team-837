package fi.poo.hwo;

import com.google.common.base.Optional;

public class OwnCar {

	private final Optional<Double> ownCarThrottle;
	private final Optional<TurboState> turbo;
	private final Car.Id ownCarId;
	
	public OwnCar(Car.Id ownCarId, Optional<Double> ownCarThrottle, Optional<TurboState> turbo) {
		this.ownCarId = ownCarId;
		this.ownCarThrottle = ownCarThrottle;
		this.turbo = turbo;
	}
	
	public OwnCar setThrottle(double throttle) {
		return new OwnCar(ownCarId, Optional.of(throttle),turbo);
	}
	
	public Optional<Double> getThrottle() {
		return ownCarThrottle;
	}
	
	public OwnCar setTurbo(Optional<TurboState> turbo) {
		return new OwnCar(ownCarId, ownCarThrottle,turbo);
	}
	
	public Optional<TurboState> getTurbo() {
		return turbo;
	}
	
	public Car.Id getOwnCarId() {
		return ownCarId;
	}
}
