package fi.poo.hwo.connection;

import fi.poo.hwo.Car.Id;
import fi.poo.hwo.protocol.CarPositions;
import fi.poo.hwo.protocol.Crash;
import fi.poo.hwo.protocol.GameInit;
import fi.poo.hwo.protocol.TurboAvailable;

public interface MsgReceiver {

	void gameStarted(long timePoint);

	void gameEnded(long timePoint);

	void ownCar(Id id);
	
	void gameInitialized(GameInit initMsg, long timePoint);

	void carPositionsReceived(CarPositions carPositions, long timePoint);

	void ownThrottleChanged(double throttle);
	
	void turboEnabled();

	void turboAvailable(TurboAvailable fromJson, long timePoint);
	
	void crash(Crash carMsg, long timePoint);

	void spawn(Crash carMsg, long timePoint);

}
