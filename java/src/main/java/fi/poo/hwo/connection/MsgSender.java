package fi.poo.hwo.connection;

public interface MsgSender {
	
	void ping();
	
	void join();
	
	void throttle(double throttle);

	void changeLaneLeft();

	void changeLaneRight();

	void turbo();
}
