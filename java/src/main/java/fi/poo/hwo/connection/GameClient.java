package fi.poo.hwo.connection;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.common.base.Optional;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import fi.poo.hwo.protocol.CarPositions;
import fi.poo.hwo.protocol.Crash;
import fi.poo.hwo.protocol.Disqualified;
import fi.poo.hwo.protocol.GameInit;
import fi.poo.hwo.protocol.Join;
import fi.poo.hwo.protocol.Ping;
import fi.poo.hwo.protocol.SendMsg;
import fi.poo.hwo.protocol.SwitchLane;
import fi.poo.hwo.protocol.Throttle;
import fi.poo.hwo.protocol.Turbo;
import fi.poo.hwo.protocol.TurboAvailable;
import fi.poo.hwo.protocol.YourCar;
import fi.poo.hwo.tools.Actor;

public class GameClient extends Actor implements MsgSender {

	private final MsgReceiver msgReceiver;
	private final String botName;
	private final String botKey;

	private Optional<Socket> socket = Optional.absent();
	private Optional<PrintWriter> writer = Optional.absent();
	private Optional<MsgReaderImpl> reader = Optional.absent();

	public GameClient(MsgReceiver msgReceiver, String botName, String botKey) {
		this.botName = botName;
		this.botKey = botKey;
		this.msgReceiver = msgReceiver;
	}

	public ListenableFuture<?> connect(final String host, final int port) {
		return submit(new Runnable() {
			@Override
			public void run() {
				System.out.println("Connecting client " + botName + "["
						+ botKey + "] to " + host + ":" + port);
				try {
					socket = Optional.of(new Socket(host, port));
					writer = Optional.of(new PrintWriter(
							new OutputStreamWriter(socket.get()
									.getOutputStream(), "utf-8")));
					reader = Optional.of(new MsgReaderImpl(new BufferedReader(
							new InputStreamReader(
									socket.get().getInputStream(), "utf-8")),
							msgReceiver));
					reader.get().start().addListener(new Runnable() {

						@Override
						public void run() {
							disconnect();
						}
					}, getExecutor());
				} catch (Throwable t) {
					doDisconnect();
					throw new RuntimeException(t);
				}
			}
		});
	}

	public void disconnect() {
		submit(new Runnable() {
			@Override
			public void run() {
				doDisconnect();
			}
		});
	}

	@Override
	public void join() {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new Join(botName, botKey));
			}
		});
	}

	@Override
	public void throttle(final double throttle) {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new Throttle(throttle));
				msgReceiver.ownThrottleChanged(throttle);
			}
		});
	}

	@Override
	public void turbo() {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new Turbo());
				msgReceiver.turboEnabled();
			}
		});
	}

	@Override
	public void changeLaneLeft() {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new SwitchLane("Left"));
			}
		});
	}

	@Override
	public void changeLaneRight() {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new SwitchLane("Right"));
			}
		});
	}

	@Override
	public void ping() {
		submit(new Runnable() {
			@Override
			public void run() {
				send(new Ping());
			}
		});
	}

	private void doDisconnect() {
		System.out.println("Disconnecting client");
		if (writer.isPresent()) {
			close(writer.get());
		}
		writer = Optional.absent();
		if (reader.isPresent()) {
			close(reader.get());
		}
		reader = Optional.absent();
		if (socket.isPresent()) {
			close(socket.get());
		}
		socket = Optional.absent();
	}

	private void send(final SendMsg msg) {
		if (writer.isPresent()) {
			writer.get().println(msg.toJson());
			writer.get().flush();
		} else {
			throw new IllegalStateException(
					"Connection not established. Cannot send messages!");
		}
	}

	private void close(Closeable stream) {
		if (stream != null) {
			try {
				stream.close();
			} catch (Exception e) {
				System.err.println("Failed closing stream " + stream);
				e.printStackTrace();
			}
		}
	}

	private static final class MsgReaderImpl extends Actor implements Closeable {
		private final Gson gson = new Gson();
		private final BufferedReader reader;
		private final MsgReceiver receiver;

		public MsgReaderImpl(BufferedReader reader, MsgReceiver receiver) {
			this.reader = reader;
			this.receiver = receiver;
		}

		public ListenableFuture<?> start() {
			return submit(new Runnable() {
				@Override
				public void run() {
					String line = null;
					long start = System.currentTimeMillis();

					try {
						while ((line = reader.readLine()) != null) {
							final MsgWrapper msgFromServer = gson.fromJson(
									line, MsgWrapper.class);
							long timePoint = System.currentTimeMillis() - start;
							if (msgFromServer.msgType.equals("carPositions")) {
								receiver.carPositionsReceived(
										gson.fromJson(line, CarPositions.class),
										timePoint);
							} else if (msgFromServer.msgType.equals("yourCar")) {
								receiver.ownCar(gson.fromJson(line,
										YourCar.class).getId());
							} else if (msgFromServer.msgType.equals("gameInit")) {
								receiver.gameInitialized(
										gson.fromJson(line, GameInit.class),
										timePoint);
							} else if (msgFromServer.msgType.equals("gameEnd")) {
								// TODO: Parse
								receiver.gameEnded(timePoint);
							} else if (msgFromServer.msgType.equals("gameStart")) {
								receiver.gameStarted(timePoint);
							} else if (msgFromServer.msgType
									.equals("turboAvailable")) {
								receiver.turboAvailable(gson.fromJson(line,
										TurboAvailable.class), timePoint);
							} else if (msgFromServer.msgType.equals("crash")) {
								receiver.crash(
										gson.fromJson(line, Crash.class),
										timePoint);
							} else if (msgFromServer.msgType.equals("spawn")) {
								receiver.spawn(
										gson.fromJson(line, Crash.class),
										timePoint);
							} else if(msgFromServer.msgType.equals("dnf")) {
								Disqualified dnf = gson.fromJson(line, Disqualified.class);
								System.out.println("Disqualified car: " + dnf.getCarId() + " Reason: " + dnf.getReason());
							}else {
								System.err
										.println("Server sent us this carbage; "
												+ msgFromServer != null ? "Type " +  msgFromServer.msgType 
												+ " Data: " + msgFromServer.data : msgFromServer);
							}
						}
					} catch (JsonSyntaxException | IOException e) {
						System.err.println("JSON read failed");
						e.printStackTrace();
						throw new RuntimeException(e);
					}
				}
			});
		}

		@Override
		public void close() throws IOException {
			reader.close();
		}
	}

	public static class MsgWrapper {
		public final String msgType;
		public final Object data;

		public MsgWrapper(final String msgType, final Object data) {
			this.msgType = msgType;
			this.data = data;
		}
	}
}
