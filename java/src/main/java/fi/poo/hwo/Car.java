package fi.poo.hwo;

import com.google.gson.annotations.SerializedName;

public class Car {
	private final Id id;
	private final Dimension dimension;

	public Car(Id id, Dimension dimension) {
		this.id = id;
		this.dimension = dimension;
	}

	public boolean is(Car.Id id) {
		return this.id.equals(id);
	}

	public String getColor() {
		return id.color;
	}

	public String getName() {
		return id.name;
	}

	public double getLength() {
		return dimension.length;
	}

	public double getWidth() {
		return dimension.width;
	}

	public Id getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Driver: " + id.name + " Color: " + id.color;
	}

	public static class Id {

		@SerializedName("name")
		private final String name;
		@SerializedName("color")
		private final String color;

		public Id(String name, String color) {
			this.name = name;
			this.color = color;
		}

		public String getName() {
			return name;
		}

		public String getColor() {
			return color;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((color == null) ? 0 : color.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Id other = (Id) obj;
			if (color == null) {
				if (other.color != null)
					return false;
			} else if (!color.equals(other.color))
				return false;
			if (name == null) {
				if (other.name != null)
					return false;
			} else if (!name.equals(other.name))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return name + " (" + color + ")";
		}

	}

	public static class Dimension {

		private final double length;
		private final double width;

		public Dimension(double length, double width) {
			this.length = length;
			this.width = width;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			long temp;
			temp = Double.doubleToLongBits(length);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			temp = Double.doubleToLongBits(width);
			result = prime * result + (int) (temp ^ (temp >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Dimension other = (Dimension) obj;
			if (Double.doubleToLongBits(length) != Double
					.doubleToLongBits(other.length))
				return false;
			if (Double.doubleToLongBits(width) != Double
					.doubleToLongBits(other.width))
				return false;
			return true;
		}

	}

}
