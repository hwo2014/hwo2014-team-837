package fi.poo.hwo;

public class Lane {
	private final double distanceFromCenter;
	private final int index;
	
	public Lane(double distanceFromCenter,int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
	
	public int getIndex() {
		return index;
	}
	
	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}
}
